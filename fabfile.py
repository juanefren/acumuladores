#!/usr/bin/env python
from fabric import Connection, task
server_conn = Connection('root@s0.admintotal.com')

virtualenv = '/srv/virtualenvs/acumuladores'
python_pip = '{}/bin/pip'.format(virtualenv)
python = '{}/bin/python'.format(virtualenv)
repo = '/srv/Projects/acumuladores'

@task(optional=['log'])
def deploy(context):
	print("Actualizando repositorio")
	server_conn.run('cd {};git pull'.format(repo))

	print("Actualizando dependencias (requirements.txt)")
	server_conn.run('{} install -r {}/requirements.txt -q'.format(python_pip, repo))

	print("Ejecutando migraciones")
	server_conn.run('{} {}/manage.py migrate'.format(python, repo))


	print("Static files")
	server_conn.run('{} {}/manage.py collectstatic --noinput'.format(python, repo))

	print("Reiniciando uWSGI")
	server_conn.run(f'touch {repo}/reload_uwsgi')

	print("Finalizado.")
