# Acumuladores

## Introducción
¿Qué es un casco? Cuando se repara un vehículo, a la refacción antigua o con fallas que fue reemplazada se le llama casco. Estos cascos son posteriormente remanufacturados (reciclados) por compañías de acuerdo a las especificaciones originales del equipo.

Este proyecto integra a empresas que venden acumuladores o baterías de automóviles a través de Admintotal, para poder tener un control sobre los cascos de baterías y poder decidir enviarlos el proveedor.

Este proyecto hace lo siguiente:
    - Debe poder tener un catálogo de cascos, solo es necesario guardar el cdigo y valor_proporcional  

    - Poder agregar entradas o salidas de cascos manualmente.
        
    - Debe consultar el endpoint /api/v2/control_cascos/ y los movimientos.

    - Mostrar el kárdex de cada casco, (una reporte con todas las entradas y/o salidas de los cascos ordenadas por fecha)

## Instalación
Aquí se encuentran las instrucciones para dejar corriendo una copia de Homedepot en tu máquina local y como aplicar actualizaciones al servidor de pruebas.
### Pre-requisitos
```
Python 3.6 o superior
Git (cualquier versión que sea compatible con Gitlab)
```
## Instalación en Linux
### Instalar virtualenv
```
sudo apt-get install python3-pip
sudo pip3 install virtualenv
```
### Instalar postgresql
```
sudo apt-get install postgresql
```
### Crear un nuevo virtualenv
Creamos un virtualenv para el proyecto, para este caso usaremos la carpeta ~/.virtualenvs pero puedes utilizar cualquier otra ruta en caso de que ya manejes algún otro directorio para guardar tus virtualenvs.
```
mkdir ~/.virtualenvs
python3 -m venv ~/.virtualenvs/app
```
### Clonar proyecto
Primeramente clonamos el proyecto de Github y los almacenamos en cualquier carpeta, en este caso estamos creando el proyecto dentro de una carpeta **Projects** dentro **home** (~)
```
mkdir ~/Projects
cd ~/Projects
git clone git@gitlab.com:admintotal/app.git
```
### Instalación de dependencias
Activamos el virtualenv recién creado:
```
source ~/.virtualenvs/app/bin/activate
```
Accedemos a la carpeta del nuevo proyecto e instalamos las dependencias:
```
cd ~/Projects/app
pip install  -r requirements.txt
```
Si nos arroja algún error de compilación al tratar de instalar el paquete **psycopg2** durante el último comando, probemos instalar los siguientes paquetes:
```sudo apt-get install libpq-dev python-dev```
y ejecutar nuevamente ```pip install  -r requirements.txt```
### Base de datos
Dentro del proyecto encontraremos otra carpeta **workflow**, dentro de esa otra carpeta copiamos el archivo genérico settings_copy.py a settings.py
```
cp app/settings_copy.py app/settings.py
```
El archivo settings_copia.py viene configurado con el usuario postgres y sin contraseña, En caso de necesitar alguna configuración en especial pueden hacerla en ese archivo por ejemplo, si necesitan especificar un password para la base de datos o si por algún motivo necesitan usar un usuario distinto de base de datos, pueden agregar alguna de las siguientes líneas al final del nuevo archivo copiado en workflow/settings.py
```
DATABASES["default"]["USER"]  = "miusuario"
DATABASES["default"]["PASSWORD"]  = "123456"
```
Creamos una nueva base de datos y generamos las tablas.
```
createdb acumuladores
```
Con la base de datos lista, hay que crear un tenant
```
python manage.py migrate
python manage.py create_tenant
```

```
python manage.py migrate
python manage.py makemigrations

```

Al preguntar por schema name y domain ponemos "bos" y "bos.local" y agregamos ese dominio a /etc/hosts

```
echo '127.0.0.1 bos.local' | sudo tee -a /etc/hosts
```

Creamos un superusuario
```
python manage.py tenant_command createsuperuser -s bos
```

### Arrancar servidor local
Finalmente corremos el servidor web en nuestra máquina:
```
python manage.py runserver
```
Para probar que haya funcionado correctametne hay que teclear en la barra de URL de nuestro navegador:
```
http://bos.local:8000/
```

## Modificar archivo CSS
Para trabajar con CSS primeramente se instala NodeJS, para ello podemos ejectuar el comando:
```
sudo apt-get install nodejs
```
Después nos vamos a la carpeta del proyecto y ejecutamos:
```
npm install
```
Una vez que finalice ejecutan el comando:
```
npm run watch
```
El archivo ```app/static/app/scss/app.scss``` contiene los estilos personalizados para el proyecto.
El archivo ```app/static/app/scss/custom.scss``` contiene la información para sustituir clas
## Consideraciones adicionales
Una vez instalado el proyecto solo habrá que activar el virtualenv y correr el servidor para volverlo a levantar
```
cd ~/Projects/app
source ~/.virtualenvs/app/bin/activate
python manage.py runserver
```
Si alguna actualización requiere algún cambio en la base de datos será necesario ejecutar nuevamente  ```python manage.py migrate.py```
De la misma manera si incluye nuevas dependencias será necesario ejecutar nuevamente ```pip install -r requirements.txt```
Hay que utilizar la guía [PEP-8](https://www.python.org/dev/peps/pep-0008/) aquí hay una versión en [español](http://www.recursospython.com/pep8es.pdf), en particular es importante poner atención en estos 2 puntos particulares:
- Hay que utilizar 4 caracteres para la indentación.
- No hacer ningún import *  a menos que sea consultado previamente por el equipo de desarrollo.
## Deployment
Para aplicar las actualizar se puede hacer desde https://jenkins.admintotal.com o bien se puede correr con el comando (es necesario tener credenciales de ssh apropiadas para esta segunda opción):
```
fab deploy
```
## Pruebas automatizadas
Descargar [geckodriver](https://github.com/mozilla/geckodriver/releases) y copiarlo a ```/usr/local/bin```
Por ejemplo si lo descargaste en ```~/Downloads```
```
sudo cp ~/Downloads/geckodriver /usr/local/bin/
```
## Recursos adicionales
* [Django](https://docs.djangoproject.com/en/2.2/) - The web framework used



## Instalar para servidor web
python3 -m venv /srv/virtualenvs/acumuladores

source /srv/virtualenvs/acumuladores/bin/activate

cp /etc/nginx/sites-available/abaco /etc/nginx/sites-available/acumuladores

Abrir para editar /etc/nginx/sites-available/acumuladores, reemplazar todas las ocurrencias de "abaco" por "acumuladores" y borrar cualquier referencia de ssl y el redirect

### SSL del public
certbot --nginx
```
seleccionar el número de nuevo dominio y el tipo 2 para hacer redirect
```
### Tramitar SSL Wildcard
```
certbot certonly   --dns-google   --dns-google-credentials ~/.secrets/certbot/google.json   -d *.acumuladores.admintotal.com

cp /etc/nginx/sites-available/abc /etc/nginx/sites-available/wildcard_acumuladores
```
Copiar el .pem resultando y configurarlo aquí:
/etc/nginx/sites-available/wildcard_acumuladores
```
ln -s /etc/nginx/sites-available/acumuladores /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/wildcard_acumuladores /etc/nginx/sites-enabled/
service nginx reload
```

Editar /etc/supervisor/conf.d/uwsgi.conf y agregar configuración del nuevo sitio

cd /srv/Projects/
git clone git@gitlab.com:admintotal/acumuladores

cp /srv/Projects/abaco/uwsgi.ini /srv/Projects/acumuladores/uwsgi.ini

Editar /srv/Projects/acumuladores/uwsgi.ini
```
:%s/abaco/acumuladores/g
```

supervisorctl 
update
exit


source /srv/virtualenvs/acumuladores/bin/activate
pip install uwsgi
cp app/settings_copy.py app/settings.py
Editar settings
```
STATIC_ROOT = '/srv/www/acumuladores/static/'
MEDIA_ROOT = '/srv/www/acumuladores/media/
```

createdb acumuladores
pip install -r requirements.txt
python manage.py migrate

python manage.py runserver 0:8000
http://acumuladores.admintotal.com:8000

python manage.py collectstatic

python manage.py create_tenant
```
python manage.py tenant_command createsuperuser -s bos
```
