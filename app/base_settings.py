
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import ignore_logger

"""
Django settings for app project.

Generated by 'django-admin startproject' using Django 2.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'kfrgbzk5dgi$6^by-ukgd6uyr+((xq=v@k(y3+)etp+(qj)(ub'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Empiezan settings  que aplican para multitenants
SHARED_APPS = (
    "django_tenants",
    "tenants",
    "django.contrib.contenttypes",
)

TENANT_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'tools',
    'atshell',
    'sorl.thumbnail',
    'configuracion',
    'usuarios',
    'app',
    'django.contrib.humanize'
]

MIDDLEWARE = [
    "django_tenants.middleware.main.TenantMainMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]
DATETIME_FORMAT = "%d/%m/%Y %H:%M:%S"
INSTALLED_APPS = list(SHARED_APPS) + [
    app for app in TENANT_APPS if app not in SHARED_APPS
]
LOCAL_DEVELOPMENT = False
TENANT_MODEL = "tenants.Tenant"
TENANT_DOMAIN_MODEL = "tenants.Domain"
DATABASE_ROUTERS = ("django_tenants.routers.TenantSyncRouter",)

PUBLIC_SCHEMA_URLCONF = 'app.urls_public'
ROOT_URLCONF = 'app.urls'
# Terminan settings  que aplican para multitenants

DATABASES = {
    "default": {
        #"ENGINE": "django.db.backends.postgresql",
        "ENGINE": "django_tenants.postgresql_backend",
        "NAME":"acumuladores",
        "USER":"postgres",
        "HOST":"127.0.0.1",
        "PORT":5432,
    }
}


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'app.context_processors.custom_context',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

MEDIA_URL = "/media/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "app_media/")
RELEASE = 1
RELEASE_TIMESTAMP= "a"
GOOGLE_API_KEY= "x"
GOOGLE_APPLICATION_CREDENTIALS = os.path.join(
    BASE_DIR, 
    "gcp-service-account.json"
)
GCS_BACKUPS_BUCKET = "app-backups"
LOGIN_URL = "/usuarios/login/"
PDFGEN_URL = 'https://pdfgen.admintotal.com/api/v1/create'
URL_PROYECTO = "http://abaco.admintotal.com.mx"
PROJECT_NAME = "Acumuladores"
DOMAIN = "acumuladores.admintotal.com"
PATH_API_CASCOS = "/api/v2/control_cascos/"
VER_DETALLE_PATH = "/admin/redirect_to_view_url/"
URL_ADMINTOTAL = "https://*.admintotal.com"
LANGUAGE_CODE = "es-mx"

ignore_logger("django.security.DisallowedHost")
def sentry_before_send(event, hint):
    import datetime
    from django.db import connection

    #from django.conf import settings

    # version = "estable"

    # if "stage" in settings.VERSION:
    #     version = "stage"

    # if "dev" in settings.VERSION:
    #     version = "dev"

    # if "respaldo" in settings.VERSION:
    #     version = "respaldo"

    event['tags'] = {
        "tenant": connection.tenant.schema_name,
        "fecha": datetime.datetime.now().date().isoformat(),
        #"version": version,
    }
    return event



