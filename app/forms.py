import pdb
from django.forms import fields
from django.forms.fields import ChoiceField
from django.utils import timezone
from django import forms
from tools.forms import BootstrapModelForm, BootstrapForm
from django.forms import formset_factory, ValidationError
from tools.constants import ELEMENTOS_PAGINA

from .models import (
    Casco,
    Cliente,
    MovimientoCasco,
    MovimientoCascoDetalles,
    OrdenCompraDomicilio,
    Proveedor,
    Recolector

)
from tools.forms import CalendarDateField

from .functions import get_years

class FechaForm(BootstrapForm):
    fecha = CalendarDateField(required=False)

    

class CascoForm(BootstrapModelForm):
    class Meta:
        model = Casco
        fields = (
            "codigo",
            "valor",
        )

class ClienteForm(BootstrapModelForm):
    class Meta:
        model = Cliente
        fields = (
            "nombre",
            "saldo_inicial",
            "fecha_saldo_inicial",
            "es_proveedor",
            "telefono",
            "costo_venta_casco",
            "costo_compra_casco",
        )
    fecha_saldo_inicial = CalendarDateField(required=False)

class MovimientoCascoForm(BootstrapModelForm):

    fecha = CalendarDateField(required=True)
    class Meta:
        model = MovimientoCasco
        fields = (
            "fecha",
            "tipo",
            "cliente",
            "numero_vale",
            "recolector"
        )
    
    def clean(self):
        data = self.cleaned_data
        fecha = data.get("fecha")
        tipo = data.get("tipo")
        cliente = data.get("cliente")
        numero_vale = data.get("numero_vale")
        recolector = data.get("recolector")

        if not self.cleaned_data["tipo"]:
            raise forms.ValidationError({
                "tipo": (
                    "Favor de un tipo de movimiento"
                )
        })
        if not self.cleaned_data["cliente"]:
            raise forms.ValidationError({
                "cliente": (
                    "Favor de seleccionar un cliente"
                )
        })
        if not self.cleaned_data["numero_vale"]:
            raise forms.ValidationError({
                "numero_vale": (
                    "Favor de ingresar un numero de vale"
                )
        })
        if not self.cleaned_data["recolector"]:
            raise forms.ValidationError({
                "recolector": (
                    "Favor de seleccionar un recolector"
                )
        })

        return data

class MovimientoCascoDetallesFS(BootstrapModelForm):
    class Meta:
        model = MovimientoCascoDetalles
        fields = (
            "cantidad_origen_local",
            "casco",
        )

class FiltroClientesForm(BootstrapForm):

    q = forms.CharField(max_length=100, required=False)
    page_size = forms.ChoiceField(choices=ELEMENTOS_PAGINA, required=False)
    tipo = forms.ChoiceField(
        choices=(
            ("", "---"),
            ("2", "Solo proveedores"),
            ("1", "Solo clientes"),
        ),
        required=False,
    )
    

class FiltroForm(BootstrapForm):
    MESES = (
        (1, "Enero"),
        (2, "Febrero"),
        (3, "Marzo"),
        (4, "Abril"),
        (5, "Mayo"),
        (6, "Junio"),
        (7, "Julio"),
        (8, "Agosto"),
        (9, "Septiembre"),
        (10, "Octubre"),
        (11, "Noviembre"),
        (12, "Diciembre"),
    )
    
    q = forms.CharField(max_length=100, required=False)
    desde = CalendarDateField(required=False)
    hasta = CalendarDateField(required=False)
    meses = forms.ChoiceField(choices=MESES,required = False)
    years = forms.ChoiceField( required=False)
    casco = forms.ModelChoiceField(required=False, queryset=Casco.objects.all())
    cliente = forms.ModelChoiceField(required=False, queryset=Cliente.objects.all())
    page_size = forms.ChoiceField(choices=ELEMENTOS_PAGINA, required=False)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        hoy = timezone.now()

        if self.data.get('desde') is None:
            self.data["desde"] = hoy.date()

        if self.data.get('hasta') is None:
            self.data["hasta"] = hoy.date()
        
        self.fields["years"].choices = get_years()
    
        
        #current_class = self.fields["casco"].widget.attrs.get("class", "")
        #self.fields["casco"].widget.attrs["class"] = " ".join((current_class, "selectpicker"))
        #self.fields["casco"].widget.attrs["data-live-search"] = "true"

        f = self.fields["cliente"]
        current_class = f.widget.attrs.get("class", "")
        f.widget.attrs["class"] = " ".join((current_class, "selectpicker"))
        f.widget.attrs["data-width"] = "400"
        f.widget.attrs["data-live-search"] = "true"

class OrdenCompraDomicilioForm(BootstrapModelForm):

    cliente_ac = forms.CharField()
    telefono_ac = forms.CharField()

    class Meta:
        model = OrdenCompraDomicilio
        fields = (
            "numero_pedido",
            "medio",
            "fecha_de_recepcion",
            "requiere_factura",
            "datos_facturacion_tecnico",
            "cliente",
            "tipo_servicio",
            "entrega_usado",
            "marca_automovil",
            "modelo_automovil",
            "ano_automovil",
            "observaciones",
            "tipo_pago",
            "requieres_cambio",
            "monto_cambio",
            "meses_sin_intereses",
            "tecnico",
            "dia",
            "hora",
            "agente_telefonico",
            "calle",
            "colonia",
            "no_i",
            "no_e",
            "cp",
            "municipio",
            "estado",
            "referencias",
            "estacionamiento",
        )
class MovimientoCascoForm(BootstrapModelForm):
    cliente_ac = forms.CharField()
    recolector = forms.ModelChoiceField(queryset = Recolector.objects.all(), to_field_name = 'nombre', empty_label="----")
    class Meta:
        model = MovimientoCasco
        fields = (
            "folio",
            "tipo",
            "cliente",
            "fecha",
            "descuento",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["tipo"].required=False
        self.fields["cliente"].required=False
        self.fields["cliente_ac"].required=True
        self.fields["descuento"].required=True
        self.initial["descuento"]=0


class SalidaProveedorForm(BootstrapModelForm):
    class Meta:
        model = MovimientoCasco
        fields = (
            "proveedor",
            "fecha",
            "folio",
            "almacen",
        )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["folio"].required = True
        self.fields["fecha"].required = True
        self.fields["proveedor"].required = True
        self.fields["almacen"].required = True


class DetallesSalidaProveedorForm(BootstrapForm):
    folio = forms.CharField()
    
    def __init__(self, *args, proveedor, **kwargs):
        self.proveedor = proveedor
        super().__init__(*args, **kwargs)
        
    def clean_folio(self):
        folio = self.cleaned_data['folio']
        if not MovimientoCasco.objects.filter(folio=folio).exists():
            raise ValidationError("Este folio es inexistente")
        return folio


DetallesSalidaProveedorFS = formset_factory(
    form=DetallesSalidaProveedorForm,
    extra=1,
)
    


