window.alertify = {
    container: '#alertifyContainer',
    closeTimeout: 6000,

    setCloseEvent: function($element) {
        setTimeout(function() {
            $element.slideUp('slow', function() {
                // $element.remove();
            })
        }, this.closeTimeout)
    },

    setEvents: function($message) {
        $message.click(function() {
            $(this).slideUp('slow')
        })
    },

    error: function(msg) {
        var $message = $('<div class="alertify alertify-danger">'+msg+'</div>')
        this.setEvents($message)
        $(this.container).append($message)
        this.setCloseEvent($message)
    },

    success: function(msg) {
        var $message = $('<div class="alertify alertify-success">'+msg+'</div>')
        this.setEvents($message)
        $(this.container).append($message)
        this.setCloseEvent($message)
    },

    info: function(msg) {
        var $message = $('<div class="alertify alertify-info">'+msg+'</div>')
        this.setEvents($message)
        $(this.container).append($message)
        this.setCloseEvent($message)
    },

    alert: function(msg, cb) {
        
        $("#alertifyConfirmModal .modal-body").html(msg)
        $("#alertifyConfirmModal .btn-confirm-continuar").off().click(function(e) {
            $("#alertifyConfirmModal").modal("hide")    
            if ( typeof cb == "function"){
                cb(true)
            }      
        })
        $("#alertifyConfirmModal").modal()
        $("#btnCerrarModal").hide();
        
    },

    confirm: function(msg, cb) {
        $("#btnCerrarModal").show();
        if (cb && typeof cb === "function") {
            $("#alertifyConfirmModal .modal-body").html(msg)
            $("#alertifyConfirmModal .btn-confirm-continuar").off().click(function(e) {
                $("#alertifyConfirmModal").modal("hide")    
                
                cb(true)
            })
            $("#alertifyConfirmModal .btn-confirm-cerrar").off().click(function(e) {
                $("#alertifyConfirmModal").modal("hide")    
                cb(false)
            })

            $("#alertifyConfirmModal").modal()
        }
    },

    prompt: function(msg, cb, opts) {
        $("#btnCerrarModal").show();
        if (cb && typeof cb === "function") {
            var $prompt = $('<div class="form-group"></div>')
            opts = opts || {}
            var type = opts.type || "text"
            $prompt.append('<label>'+ msg +'</label>')
            $prompt.append('<input type="'+type+'" class="form-control prompt-value" required />')
            $("#alertifyConfirmModal .modal-body").html($prompt)
            $("#alertifyConfirmModal .btn-confirm-continuar").off().click(function(e) {
                $("#alertifyConfirmModal").modal("hide")    
                cb($prompt.find(".prompt-value").val())
            })
            $("#alertifyConfirmModal .btn-confirm-cerrar").off().click(function(e) {
                $("#alertifyConfirmModal").modal("hide")
            })

            $("#alertifyConfirmModal").modal()
        }
    },

    setConfig: function(config) {
    	for(var k in config) {
    		this[k] = config[k]
    	}
    },
}