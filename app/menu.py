from django.urls import reverse

MENU = [
    {
        "nombre": "Reportes",
        "icono": "icon ion-md-analytics",
        "id": "definiciones",
        "children": [
            {"id": "app:kardex", "nombre": "Kárdex", },
            {"id": "app:kardex_garantia", "nombre": "Garantías", },
            {"id": "app:kardex_entradas_almacen", "nombre": "Entradas almacen", },
            {"id": "app:salidas_proveedor", "nombre": "Salidas a proveedor", },
            {"id": "app:contador_usados", "nombre": "Contador de usados", },
            {"id": "app:notas_credito", "nombre": "Notas de crédito", },
            {"id": "app:kardex_almacen", "nombre": "Inventario almacen", },
            # {"id": "app:graficas_movimientos", "nombre": "Graficas de movimientos", },
        ],
    },
    # {
    #     "nombre": "Ordenes compra",
    #     "icono": "icon ion-md-analytics",
    #     "id": "orden_compra",
    #     "children":[
    #         {"id": "app:ordenes_compra_domicilio", "nombre":"A Domicilio",},
    #     ]
    # },
    {
        "nombre": "Administración",
        "icono": "icon ion-md-analytics",
        "id": "configuracion",
        "children": [
            {"id": "app:catalogo_clientes", "nombre": "Catálogo de clientes", },
            {"id": "app:catalogo_cascos", "nombre": "Catálogo de cascos", },
            {"id": "usuarios:usuarios", "nombre": "Catálogo de Usuarios", },
            {"id": "app:configuracion_inventario", "nombre": "Configuración inventario", },
            {"id": "configuracion:configuracion", "nombre": "Configuración general", },
            
        ],
    },
]
