from .menu import MENU
from django.conf import settings
from configuracion.models import Configuracion
from django.conf import settings


def custom_context(request):
    """
    Default context para todos los requests de la aplicación.
    """
    #configuracion = Configuracion.objects.get()
    return {
        "MENU": MENU,
        "VERSION": settings.RELEASE,
        "VERSION_TIMESTAMP": settings.RELEASE_TIMESTAMP,
        "GOOGLE_API_KEY": settings.GOOGLE_API_KEY,
        "settings":settings,
        "template_base":"app/base.html",
        "configuracion":Configuracion.objects.first(),

    }

def pdf_context(request):
    """
    Default context para los renders de pdf.
    """
    context = custom_context(request)
    context["mostrar_cabecera_pdf"] = True
    return context

def email_context(request):
    """
    Default context para los renders de pdf.
    """
    context = custom_context(request)
    return context