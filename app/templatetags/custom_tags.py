from django import template
from django.utils.safestring import mark_safe
from tools.webcolor import WebColor
from tools.constants import MESES
from django.urls import reverse
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.humanize.templatetags.humanize import intcomma
from tools.functions import to_precision_decimales, safe_division, to_decimal
import json

register = template.Library()


@register.filter
def rgb(value):
    try:
        value = WebColor(
            "#" + value if not value.startswith("#") else value
        ).copy()
        return value
    except:
        return WebColor("#000000")


@register.filter
def lightness(value, arg):
    value = value.copy()
    arg = arg.strip()
    if arg.endswith("%"):
        arg = float(arg[:-1]) / 100.0
        value.l *= arg
    else:
        arg = float(arg)
        value.l = arg
    return value


@register.filter
def saturation(value, arg):
    value = value.copy()
    arg = arg.strip()
    if arg.endswith("%"):
        arg = float(arg[:-1]) / 100.0
        value.s *= arg
    else:
        arg = float(arg)
        value.s = arg
    return value


@register.simple_tag
def HSL(h, s, l):
    try:
        h = float(h)
        s = float(s)
        l = float(l)
    except ValueError:
        raise ValueError("HSL requires a color in <H>,<L>,<S> format")
    c = WebColor.from_HSL(h, s, l)
    return str(c)


@register.simple_tag
def colorblend(col1, col2, interpolant):
    col1 = WebColor(col1)
    col2 = WebColor(col2)
    i = float(interpolant)
    col = col1.blend(col2, i)
    return str(col)


def easy_tag(func):
    """deal with the repetitive parts of parsing template tags"""

    def inner(parser, token):
        try:
            return func(*token.split_contents())
        except TypeError:
            raise template.TemplateSyntaxError(
                'Bad arguments for tag "%s"' % token.split_contents()[0]
            )

    inner.__name__ = func.__name__
    inner.__doc__ = inner.__doc__
    return inner


class AppendGetNode(template.Node):
    def __init__(self, dict, complete):
        self.dict_pairs = {}
        self.complete = int(complete)
        for pair in dict.split(","):
            pair = pair.split("=")
            self.dict_pairs[pair[0]] = template.Variable(pair[1])

    def render(self, context):
        get = context["request"].GET.copy()

        for key in self.dict_pairs:
            get[key] = self.dict_pairs[key].resolve(context)

        from django.db.models import Model

        path = context["request"].META["PATH_INFO"]
        if not self.complete:
            path = ""

        lista = []
        for key in get.keys():
            for value in get.getlist(key):
                lista.append(
                    "%s=%s"
                    % (
                        key,
                        (value if not isinstance(value, Model) else value.id),
                    )
                )

        if len(get):
            path += "?%s" % "&".join(lista)

        return path


@register.tag()
@easy_tag
def append_to_get(_tag_name, dict, complete=1):
    return AppendGetNode(dict, complete)


@register.filter
def jsonify(json_obj):
    if isinstance(json_obj, dict().values().__class__):
        json_obj = list(json_obj)
    return mark_safe(json.dumps(json_obj))


@register.filter
def rol_tiene_permiso(rol, permiso):
    if not rol.permisos:
        return False
    return permiso in rol.permisos


@register.filter
def usuario_tiene_permiso(usuario, permiso):
    return usuario.perfil.tiene_permiso(permiso)

@register.filter
def cur(value, sign=False):
    """    
    """
    if value == "":
        return ""

    str_value = intcomma("%s" % to_precision_decimales(value, 2))
    prefix = postfix = ""
    if sign == "$":
        prefix = sign

    elif sign == "%":
        postfix = sign

    return "%s%s%s" % (prefix, str_value, postfix)

@register.filter
def per(value, decimales=2):
    """    
    """
    if value == "":
        return ""

    value = to_decimal(value) * 100
    str_value = intcomma("%s" % to_precision_decimales(value, decimales))

    return "%s%%" % (str_value)


@register.filter
def diferencia(numerador, denominador):
    """    
    """
    value = safe_division(numerador, denominador) * 100
    str_value = intcomma("%s" % to_precision_decimales(value, 2))
    
    return "%s%%" % (str_value)

@register.filter
def porcentaje(numerador, denominador):
    """    
    """
    value = safe_division(numerador, denominador) * 100
    str_value = intcomma("%s" % to_precision_decimales(value, 2))
    
    return "%s%%" % (str_value)

@register.filter
def sino(b):
    if b:
        return "Sí"
    else:
        return "No"

@register.filter
def month_display(month_number):
    try:
        month_number = int(month_number)
        assert month_number > 0
    except (ValueError, AssertionError) as e:
        return "NA"

    return MESES[month_number-1][1]


@register.filter
def cr_info(crid):
    from cba.models import CentroResponsabilidad
    return CentroResponsabilidad.objects.get(id=crid).numero
        

@register.filter
def escenario_display(escenario):
    if str(escenario) == "1":
        return "Real"
    if str(escenario) == "2":
        return "Presupuesto"
        
    return "NA"
    #return MESES[month_number-1][1]