from .base_settings import *
import getpass

DEBUG = True
ALLOWED_HOSTS = ['*']
RAVEN_CONFIG = {}
username = getpass.getuser()
CRONTAB_USER =  username
LOCAL_DEVELOPMENT = True
URL_PROYECTO = "http://localhost:8000"

DATABASES["default"]["USER"]  = "miusuario"
DATABASES["default"]["PASSWORD"]  = "123456"
