from django.core.management.base import BaseCommand
from django.conf import settings
from google.cloud import storage
from os.path import expanduser
import datetime
import subprocess
import glob
import os


class Command(BaseCommand):
    help = "Crea un respaldo de la base de datos."

    def add_arguments(self, parser):
        parser.add_argument("--upload_gcs", nargs="?", type=int, default=0)

    def upload_gcs(self, directory, *, remote_dir):
        """
        Sube todos los archivos .sql del directorio especificado.
        """
        self.stdout.write("*" * 30)
        self.stdout.write("Subiendo archivos a GCP")
        self.stdout.write("*" * 30)
        google_app_credentials = getattr(
            settings, "GOOGLE_APPLICATION_CREDENTIALS", None
        )

        if not google_app_credentials:
            google_app_credentials = os.environ.get(
                "GOOGLE_APPLICATION_CREDENTIALS"
            )

        if not google_app_credentials:
            return self.stdout.write(
                self.style.ERROR(
                    "La variable GOOGLE_APPLICATION_CREDENTIALS no "
                    "se encuentra definida en el sistema ni en settings.py."
                )
            )

        if not os.path.exists(google_app_credentials):
            return self.stdout.write(
                self.style.ERROR(
                    "El archivo {} no existe.".format(google_app_credentials)
                )
            )

        storage_client = storage.Client.from_service_account_json(
            google_app_credentials
        )
        bucket = storage_client.get_bucket(settings.GCS_BACKUPS_BUCKET)

        for f in [
            f
            for f in glob.glob(
                directory + "**/*__pendiente.sql", recursive=True
            )
        ]:
            file_name = os.path.basename(f).replace("__pendiente", "")
            blob = bucket.blob(os.path.join(remote_dir, file_name))
            blob.upload_from_filename(f)
            self.stdout.write(self.style.SUCCESS(blob.path))
            os.rename(f, f.replace("__pendiente", ""))

    def handle(self, *args, **options):
        db_name = settings.DATABASES.get("default")["NAME"]
        db_user = settings.DATABASES.get("default")["USER"]
        db_host = settings.DATABASES.get("default")["HOST"]
        db_password = settings.DATABASES.get("default")["PASSWORD"]
        db_port = settings.DATABASES.get("default")["PORT"]

        homedir = expanduser("~")
        backups_folder = datetime.datetime.now().strftime("%Y-%m-%d")
        output_dir = os.path.join(homedir, "wf-backups", backups_folder)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_file = os.path.normpath(
            "{}/homedepot__pendiente.sql".format(output_dir)
        )
        command = [
            "PGPASSWORD={}".format(db_password),
            "pg_dump",
            "-U",
            db_user,
            "-h",
            db_host,
            "--format=custom",
            db_name,
            ">",
            output_file,
        ]

        try:
            output = subprocess.check_output(
                " ".join(command), stderr=subprocess.STDOUT, shell=True
            )
            self.stdout.write(
                self.style.SUCCESS(
                    "{}".format(output_file)
                )
            )
        except Exception as e:
            self.stdout.write(self.style.ERROR("ERROR:"))
            self.stdout.write("Comando: {}".format(" ".join(command)))
            self.stdout.write(self.style.ERROR(e.output.decode("utf-8")))

        if options["upload_gcs"]:
            self.upload_gcs(output_dir, remote_dir=backups_folder)

        # **
        # BORRA LOS RESPALDOS MAYORES A 7 DIAS
        # **
        clear_folder_cmd = "find {}/* -type d -ctime +{} -exec rm -rf {{}} \\;".format(
            7, output_dir
        )
        output = subprocess.check_output(
            command, stderr=subprocess.STDOUT, shell=True
        )
