from django.core.management.base import BaseCommand, CommandError
from solicitudes.models import SolicitudCotizacion
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
import json


class Command(BaseCommand):
    help = (
        "Cambia el estado de las solicitudes de servicio "
        "vencidas a 'Designar ganador' ó 'Cancelada'."
    )

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        solicitudes_vencidas = SolicitudCotizacion.objects.filter(
            status=1, vencimiento__lt=timezone.now()
        )
        resultados = {"designar_ganador": 0, "canceladas": 0}
        for solicitud in solicitudes_vencidas:
            if solicitud.cotizaciones.filter(
                fecha_cotizada__isnull=False
            ).exists():
                # designar ganador
                solicitud.status = 2
                solicitud.sub_status = 2
                resultados["designar_ganador"] += 1
            else:
                # cancelada
                solicitud.status = 4
                resultados["canceladas"] += 1

            solicitud.save()
            solicitud.notificar_vencimiento()
