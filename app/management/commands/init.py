from django.core.management.base import BaseCommand, CommandError
from configuracion.models import Configuracion
from django.conf import settings
from django.contrib.auth.models import User
from usuarios.functions import get_perfil
from usuarios.models import Rol
from workflow.models import WorkflowConfigDetail, WorkflowConfig
from django.core.management import call_command


class Command(BaseCommand):
    help = (
        "Crea los modelos necesarios"
    )

    def add_arguments(self, parser):
        pass

    def print_divider(self):
        self.stdout.write("*" * 45)

    def crear_superusuario(self):
        usuario = User.objects.first()
        if not usuario:
            password = "1234"
            user = User()
            user.username = "admin"
            user.set_password(password)
            user.is_superuser = True
            user.is_staff = True
            user.save()
            get_perfil(user)
            msg = self.style.SUCCESS(
                "Superusuario creado correctamente\n"
            )
            self.stdout.write(msg)
            self.stdout.write(
                "usuario: {}\ncontraseña: {}".format(user.username, password)
            )
            self.print_divider()

    def crear_configuracion(self):
        config = Configuracion.objects.first()
        if not config:
            Configuracion.objects.create(nombre="Workflow")
            self.stdout.write(
                self.style.SUCCESS("Configuración creada correctamente")
            )
            self.print_divider()

    def crear_workflows(self):
        wf_solicitud_prod_serv_nombre = "solicitud_prod_serv"
        wf_solicitud_prod_serv = WorkflowConfig.objects.filter(
            nombre=wf_solicitud_prod_serv_nombre
        )
        if not wf_solicitud_prod_serv:
            contador = Rol.objects.get_or_create(nombre="Contador")[0]
            jefe_admin = Rol.objects.get_or_create(nombre="Jefe Administrativo")[0]
            jefe_rh = Rol.objects.get_or_create(nombre="Jefe RH")[0]
            gerente = Rol.objects.get_or_create(nombre="Gerente")[0]
            
            w = WorkflowConfig.objects.create(
                nombre=wf_solicitud_prod_serv_nombre
            )
            WorkflowConfigDetail.objects.get_or_create(
                workflow=w,
                rol=contador,
                orden=1,
            )
            WorkflowConfigDetail.objects.get_or_create(
                workflow=w,
                rol=jefe_admin,
                orden=2,
            )
            WorkflowConfigDetail.objects.get_or_create(
                workflow=w,
                rol=jefe_rh,
                orden=2,
            )
            WorkflowConfigDetail.objects.get_or_create(
                workflow=w,
                rol=gerente,
                orden=3,
            )
            msg = self.style.SUCCESS(
                "Workflow {} creado correctamente".format(
                    wf_solicitud_prod_serv_nombre
                )
            )
            self.stdout.write(msg)
            self.print_divider()

    def handle(self, *args, **options):
        
        call_command("migrate")
        self.print_divider()

        self.crear_configuracion()
        self.crear_superusuario()
        self.crear_workflows()

        call_command("actualizar_crontab")
        self.print_divider()

        self.stdout.write(
            "Proceso finalizado."
        )
