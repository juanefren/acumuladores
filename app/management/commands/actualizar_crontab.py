from django.core.management.base import BaseCommand, CommandError
from crontab import CronTab
from django.conf import settings

class Command(BaseCommand):
    help = (
        "Regenera los crontabs del usuario actual"
    )

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        python = getattr(settings, "PYTHON_PATH", "python")
        cron = CronTab(user=True)
        cron.remove_all()

        job = cron.new(
            command="{} {}/manage.py dbbackup --upload_gcs=1".format(python, settings.BASE_DIR), 
            comment="dbbackup"
        )
        job.setall('0 3 * * *')

        job = cron.new(
            command="{} {}/manage.py solicitudes_vencidas".format(python, settings.BASE_DIR), 
            comment="solicitudes_vencidas"
        )
        job.setall('1 0 * * *')

        job = cron.new(
            command="{} {}/manage.py actualizar_crontab.py".format(python, settings.BASE_DIR), 
            comment="actualizar_crontab"
        )
        job.setall('0 1 * * *')
        cron.write()
        msg = self.style.SUCCESS(
            "Crontab del usuario {} ha sido regenerado correctamente".format(
                cron.user
            )
        )
        self.stdout.write(msg)
