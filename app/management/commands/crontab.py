from django.core.management.base import BaseCommand, CommandError
from solicitudes.models import SolicitudCotizacion, SolicitudProdServ
from usuarios.models import Perfil
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
from tools.functions import delete_crontab
from workflow.models import WorkflowProcess
import json


class Command(BaseCommand):
    help = "Crontabs por tenant, es necesario especificar siempre el schema_name"

    def _notificar_fin_suplencia(self, id):
        perfil = Perfil.objects.get(id=id)
        perfil.notificar_fin_suplencia()

    def _notificar_vencimiento_wf(self, id):
        """
        Método genérico para llamar a notificar_vencimiento_aprobacion del 
        modelo relacionado con WorkflowProcess.
        """
        wf = WorkflowProcess.objects.get(id=id)
        related_obj = wf.get_related_object()
        fexists = getattr(
            related_obj, "notificar_vencimiento_aprobacion", None
        )
        if related_obj and fexists:
            related_obj.notificar_vencimiento_aprobacion()

    def _notificar_precalificacion_vencida(self, id):
        s = SolicitudCotizacion.objects.get(id=id)
        s.notificar_precalificacion_vencida()

    def _notificar_vencimiento_aprobacion(self, id):
        s = SolicitudCotizacion.objects.get(id=id)
        s.notificar_vencimiento_aprobacion()

    def add_arguments(self, parser):
        parser.add_argument("func", type=str)
        parser.add_argument("-s", "--schema", nargs="?", type=str)
        parser.add_argument("--id", nargs="?", type=int)
        parser.add_argument("--delete-crontab", nargs="?", type=str)

    def handle(self, *args, **options):
        func = getattr(self, "_{}".format(options["func"]), None)
        obj_id = options["id"]
        if not func:
            return self.stderr.write(
                "No existe la función _{}".format(options["func"])
            )
        try:
            func(obj_id)
        except Exception as e:
            raise e
        finally:
            if options["delete_crontab"]:
                delete_crontab(options["delete_crontab"])
