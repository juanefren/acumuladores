from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from django.conf import settings
from django.contrib.auth.models import User

from google.cloud import storage
from os.path import expanduser
import subprocess
import datetime
import os


class Command(BaseCommand):
    help = "Download and restore database from google bucket."

    def add_arguments(self, parser):
        parser.add_argument("--fecha", type=str)

    def download_gcs(self, *, archivo, destino):
        """
        Descarga el archivo sql de GCP.
        """
        self.stdout.write("*" * 30)
        self.stdout.write("Descargando archivo de GCP")
        self.stdout.write("*" * 30)
        google_app_credentials = getattr(
            settings, "GOOGLE_APPLICATION_CREDENTIALS", None
        )

        if not google_app_credentials:
            google_app_credentials = os.environ.get(
                "GOOGLE_APPLICATION_CREDENTIALS"
            )

        if not google_app_credentials:
            self.stdout.write(self.style.ERROR(
                "La variable GOOGLE_APPLICATION_CREDENTIALS no "
                "se encuentra definida en el sistema ni en settings.py."
            ))
            return False

        if not os.path.exists(google_app_credentials):
            return self.stdout.write(self.style.ERROR(
                "El archivo {} no existe.".format(google_app_credentials)
            ))


        nombre_archivo = os.path.basename(destino)
        carpeta_destino = destino.replace(nombre_archivo, "")
        if not os.path.exists(carpeta_destino):
            os.makedirs(carpeta_destino)
        try:
            storage_client = storage.Client.from_service_account_json(
                google_app_credentials
            )
            bucket = storage_client.get_bucket(settings.GCS_BACKUPS_BUCKET)
            blob = bucket.blob(archivo)
            blob.download_to_filename(destino)
            self.stdout.write(self.style.SUCCESS(destino))
            return True
        except Exception as e:
            self.stdout.write(self.style.ERROR(str(e)))
            return False

    def handle(self, *args, **options):
        homedir = expanduser("~")
        path_sql_temporal = "{}/.sql/homedepot.sql".format(homedir)

        db_name = settings.DATABASES.get("default")["NAME"]
        db_user = settings.DATABASES.get("default")["USER"]
        db_host = settings.DATABASES.get("default")["HOST"]
        db_password = settings.DATABASES.get("default")["PASSWORD"]
        db_port = settings.DATABASES.get("default")["PORT"]

        today_str = datetime.date.today().strftime('%Y-%m-%d')
        if options["fecha"]:
            today_str = options["fecha"]

        archivo_gcs = "{}/homedepot.sql".format(today_str)
        descarga = self.download_gcs(
            archivo=archivo_gcs, 
            destino=path_sql_temporal
        )

        if descarga:
            # borramos la base de datos
            subprocess.call(
                ["dropdb", db_name], 
                stderr=subprocess.DEVNULL,
                stdout=subprocess.DEVNULL,
            )

            subprocess.call(
                ["createdb", db_name], 
                stderr=subprocess.DEVNULL,
                stdout=subprocess.DEVNULL,
            )

            command = [
                "/usr/bin/pg_restore", 
                "-U", 
                db_user, 
                "-h", 
                db_host, 
                path_sql_temporal, 
                "-d",
                 db_name, 
                "-c"
            ]

            try:
                print(" ".join(command))
                output = subprocess.call(
                    command, 
                    stderr=subprocess.DEVNULL,
                    stdout=subprocess.DEVNULL, 
                    env={"PGPASSWORD": db_password}
                )
                self.stdout.write(
                    self.style.SUCCESS(
                        "La carga del archivo ha sido finalizada."
                    )
                )
            except Exception as e:
                self.stdout.write(self.style.ERROR("ERROR:"))
                self.stdout.write("Comando: {}".format(" ".join(command)))
                self.stdout.write(self.style.ERROR(e.output.decode("utf-8")))

        for u in User.objects.all():
            u.set_password("1234")
            u.save()
