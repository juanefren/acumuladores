from random import choices
from django.conf import settings
from django.db.models import Q, Sum
from django.db import models
from django.utils.functional import cached_property
from django.db.models.aggregates import Max, Sum
from django.db.models.base import ModelState
from django.db.models.enums import Choices
from django.db.models.fields import PositiveIntegerField
from django.forms.fields import IntegerField
from django.utils import tree
from django.utils.html import TRAILING_PUNCTUATION_CHARS
from tools.functions import to_datetime
from django.db import connection
from django.contrib.auth.models import User
from django.utils import timezone
from tools.functions import q_split
import datetime




class Casco(models.Model):
    codigo = models.CharField(max_length=255, blank=True)
    valor = models.DecimalField(
        max_digits=5,
        decimal_places=2,
    )

    class Meta:
        ordering = ("codigo",) 

    def __str__(self):
        return self.codigo

    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        return objects
    
    @cached_property
    def get_total_salidas(self):
        cascos = self.casco_detalles.filter(movimiento__tipo__in=[2,3,4]).aggregate(total=Sum("cantidad_origen_local"))
        if not cascos.get("total"):
            total = 0
        else:
            total = cascos.get("total")
        return total

    @cached_property
    def get_total_entradas(self):
        cascos = self.casco_detalles.filter(movimiento__tipo=1).aggregate(total=Sum("cantidad_origen_local"))
        if not cascos.get("total"):
            total = 0
        else:
            total = cascos.get("total")
        return total

    @cached_property
    def get_total_salidas_base1(self):
        cascos = self.get_total_salidas*self.valor
        return cascos

    @cached_property
    def get_total_entradas_base1(self):
        cascos = self.get_total_entradas*self.valor
        return cascos

    @cached_property
    def get_inventario(self):
        inv = (self.almacen.cantidad + self.get_total_entradas) - self.get_total_salidas
        
        return inv

class InventarioAlmacen(models.Model):
    casco = models.OneToOneField(Casco, related_name="almacen", on_delete=models.PROTECT)
    cantidad = models.IntegerField()

    

class Almacen(models.Model):
    id_admintotal = models.IntegerField(unique=True, null=True)
    nombre = models.CharField(max_length=255, blank=True) 
    creado = models.DateField(auto_now_add=True, null=True)

    def __str__(self) -> str:
        return self.nombre


class Proveedor(models.Model):
    id_admintotal = models.IntegerField(unique=True, null=True)
    creado = models.DateField(auto_now_add=True)
    nombre = models.CharField(max_length=255)

    def __str__(self):
        return self.nombre

class Cliente(models.Model):
    nombre = models.CharField(max_length=255, blank=True)
    id_admintotal = models.IntegerField(unique=True, null=True)
    saldo_inicial = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    fecha_saldo_inicial = models.DateTimeField(null=True)
    es_proveedor = models.BooleanField(default=False, verbose_name="Marcar como proveedor")
    telefono = models.CharField(
        max_length=100, 
        blank=False, 
        verbose_name="Núm. teléfono",
        null=True, 
    )
    rfc = models.CharField(max_length=250,blank=True)
    codigo = models.IntegerField(null=True)
    pais = models.CharField(max_length=250, null=True)
    estado = models.CharField(max_length=100, null=True)
    municipio = models.CharField(max_length=100, blank=True)
    domicilio = models.CharField(max_length=100, blank=True)
    no_i = models.CharField(max_length=100, blank=True)
    no_e = models.CharField(max_length=100, blank=True)
    referencia =models.CharField(max_length=250, null=True)
    localidad = models.CharField(max_length=100, null=True)
    municipio = models.CharField(max_length=100, blank=True)
    cp = models.CharField(max_length=5, null=True)
    email = models.EmailField(null=True)
    costo_venta_casco = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    costo_compra_casco = models.DecimalField(max_digits=12, decimal_places=2, default=0)




    def show_tipo(self):
        if self.es_proveedor:
            return "Proveedor"
            
        return "Cliente"

    @classmethod
    def search_objects(cls, request):
        objects = cls.objects.all()
        q = request.GET.get("q")
        nombre = request.GET.get("nombre")

        if q:
            objects = objects.filter(
                models.Q(nombre__icontains=q)
            )

        tipo = request.GET.get("tipo")


        if tipo == "1":
            objects = objects.filter(es_proveedor=False)

        elif tipo == "2":
            objects = objects.filter(es_proveedor=True)

        if nombre:
            objects = objects.filter(nombre=nombre)

        return objects

    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        q = vars.get("q")
        nombre = vars.get("nombre")

        if q:
            objects = objects.filter(
                models.Q(nombre__icontains=q)
            )

        tipo = vars.get("tipo")


        if tipo == "1":
            objects = objects.filter(es_proveedor=False)

        elif tipo == "2":
            objects = objects.filter(es_proveedor=True)

        if nombre:
            objects = objects.filter(nombre=nombre)

        return objects


    def __str__(self):
        return self.nombre

    def get_input_display(self):
        return "%s" % (self.nombre)

class TotalesMovimientosMeses(models.Model):
    cantidad_entrada = models.FloatField(default=0)
    cantidad_salida =models.FloatField(default=0)
    date_mes = models.CharField(max_length= 100, null=True, unique=True)

    
            


class Recolector(models.Model):
    nombre = models.CharField(max_length=200)
    id_admintotal = models.IntegerField(unique=True, null=True)

    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        return objects

    def __str__(self):
        return self.nombre

class MovimientoCasco(models.Model):

    TM_ENTRADA = 1
    TM_SALIDA = 2
    SALIDA_USADO_PROVEEDOR = 3
    SALIDA_GARANTIA_PROVEEDOR = 4

    TIPOS_MOVIMIENTO_CASCO = (
        (TM_ENTRADA, "Entrada"),
        (TM_SALIDA, "Salida"),
        (SALIDA_USADO_PROVEEDOR, "Salida usado proveedor"),
        (SALIDA_GARANTIA_PROVEEDOR, "Salida garantia proveedor"),
    )

    id_admintotal = models.IntegerField(unique=True, null=True)
    folio = models.CharField(max_length=200)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de registro" )
    fecha = models.DateField(default=datetime.date.today, verbose_name="Fecha de recolección")
    cliente = models.ForeignKey(
        Cliente, related_name="movimiento", on_delete=models.CASCADE, null=True
    )
    casco = models.ForeignKey(
        Casco, related_name="casco", on_delete=models.CASCADE, null=True, blank=True
    )
    recolector = models.ForeignKey(
        Recolector, null=True, related_name="recolector", on_delete=models.CASCADE
    )
    cantidad_origen = models.FloatField(default=0,null=True, blank=True)
    cantidad_restante = models.FloatField(null=True, blank=True, default=0)
    valor_casco = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        null=True,
    )

    tipo = models.PositiveSmallIntegerField(
        choices=TIPOS_MOVIMIENTO_CASCO
    )
    es_vale= models.BooleanField(default=False)
    numero_vale = models.IntegerField(unique=True, null=True)   
    gran_saldo = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    saldo_cliente = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    usuario_creado = models.ForeignKey(User, null=True, related_name="usuario_creado", on_delete=models.CASCADE)
    cancelado = models.BooleanField(default=False)
    url_pdf = models.CharField(max_length=255, null=True, default=False)
    pagado = models.BooleanField(default=False)
    fecha_year_mes = models.ForeignKey(TotalesMovimientosMeses, null=True, blank=True, on_delete=models.CASCADE, related_name='movimiento')
    es_garantia = models.BooleanField(default=False)
    almacen = models.ForeignKey(Almacen, related_name="movimientos_cascos", null=True,blank=True, on_delete=models.PROTECT)
    proveedor = models.ForeignKey(Proveedor, related_name="movimiento_cascos", null=True, blank=True, on_delete=models.PROTECT)
    salida_proveedor = models.ForeignKey("self", related_name="garantias", null=True, blank=True, on_delete=models.PROTECT)
    xml = models.TextField(blank=True)
    descuento = models.IntegerField(null=True)
    
    


    

    
    def es_entrada(self):
        return self.tipo == self.TM_ENTRADA

    def es_salida(self):
        return self.tipo == self.TM_SALIDA

    def total_pesos_entrada(self):
        return self.cantidad_origen * self.cliente.costo_compra_casco

    class Meta:
        ordering = ("-fecha","-id") 


    def get_valor_cascos(self):
        total =0
        for x in self.detalles.all():
            total += x.cantidad_origen_local * x.casco.valor 

        return total
    
    def get_valor_monto(self):
        total =0
        for x in self.detalles.all():
            total += x.cantidad_origen_local * x.casco.valor 

        if self.cliente.costo_compra_casco:
            total = total*self.cliente.costo_compra_casco
        else:
            total = total*250
        if self.descuento:
            total = total - ((total*self.descuento)/100)
        return total

    def get_total_valor_monto(self):
        total =0
        for x in self.detalles.all():
            total += x.cantidad_origen_local * x.casco.valor 

        if self.cliente.costo_compra_casco:
            total = total*self.cliente.costo_compra_casco
        else:
            total = total*250

        total = total+(total*0.16)

        return total

    def get_valor_monto_restante(self):
        total= self.cantidad_restante
        if not total:
            total = 0
        if self.cliente.costo_compra_casco:
            total = total*float(self.cliente.costo_compra_casco)
        else:
            total = total*250
        if self.descuento:
            total = total - ((total*self.descuento)/100)
        return abs(total)

    def get_valor_total_restante(self):
        total = self.cantidad_restante
        if not total:
            total = 0
        if self.cliente.costo_compra_casco:
            total = total*float(self.cliente.costo_compra_casco)
        else:
            total = total*250

        return float(total)+(float(total)*0.16)

    def get_total_valor_monto_restante(self):
        total= self.cantidad_restante
        if not total:
            total = 0
        if self.cliente.costo_compra_casco:
            total = total*self.cliente.costo_compra_casco
        else:
            total = total*250

        total = total+(total*0.16)
        return total
    
    def get_cantidad_por_valor_entrada(self):
        if self.fecha_year_mes:
            return self.fecha_year_mes.cantidad_entrada * float(self.cliente.costo_venta_casco)
        else:
            return ''

    def get_precio_venta(self):
        if self.fecha_year_mes:
            return self.fecha_year_mes.cantidad_salida * float(self.cliente.costo_compra_casco)

        else:
            return ''

    def get_costo_venta(self):
        if self.fecha_year_mes:
            return self.fecha_year_mes.cantidad_salida * float(self.cliente.costo_venta_casco)

        else:
            return ''

    def get_utilidad(self):
        if self.get_costo_venta() != ''  and self.get_precio_venta() != '':
            return self.get_precio_venta() - self.get_costo_venta()
        else:
            return self.get_precio_venta()

    def get_marge_u(self):
        if self.get_utilidad() != ''  and self.get_precio_venta() != 0:
            return (self.get_utilidad()/self.get_precio_venta())
        else:
            return ''
    
    def get_inv_prom(self):
        field = 'cantidad_restante'
        mov=MovimientoCasco.objects.filter(~Q(cantidad_restante= 0)).filter(Q(fecha__year__gte=self.fecha.year-1 , fecha__month__gte=self.fecha.month, fecha__month__lte=self.fecha.month , fecha__year__lte=self.fecha.year))

        if mov is None:
            return to_decimal("0")

    
        monto = mov.aggregate(suma=Sum(field))
        
        return (monto["suma"]*float(self.cliente.costo_venta_casco))/13

    def get_inventario(self):
        field = 'cantidad_restante'
        mov=MovimientoCasco.objects.filter(~Q(cantidad_restante= 0)).filter(Q(fecha__year__gte=self.fecha.year-1 , fecha__month__gte=self.fecha.month, fecha__month__lte=self.fecha.month , fecha__year__lte=self.fecha.year))

        if mov is None:
            return to_decimal("0")

    
        monto = mov.aggregate(suma=Sum(field))
        
        return monto["suma"]

    def get_coeficiente_inv(self):
        if self.get_costo_venta() != ''  and self.get_precio_venta() != 0:
            return self.get_precio_venta() / self.get_inv_prom()
        else:
            return self.get_precio_venta()

    def get_gmroi(self):
        if self.get_marge_u() != ''  and self.get_coeficiente_inv() != '':
            return self.get_marge_u() * self.get_coeficiente_inv()
        else:
            return ''

    def get_valor_cascos_restante(self):
        total =0
        for x in self.detalles.all():
            total += x.cantidad_origen_pendiente * x.casco.valor 


        return total

    def get_anualizado(self):
        field = 'fecha_year_mes__cantidad_salida'
        mov=MovimientoCasco.objects.filter(~Q(cantidad_restante= 0),cliente=self.cliente).filter(Q(fecha__year__gte=self.fecha.year-1 , fecha__month__gte=self.fecha.month, fecha__month__lte=self.fecha.month , fecha__year__lte=self.fecha.year))

        if mov is None:
            return ''

    
        monto = mov.aggregate(suma=Sum(field))
        
        return monto["suma"]*float(self.cliente.costo_venta_casco)
        
    def get_year(self):
        return self.creado.strftime('%Y')

    def get_cantidad_origen(self):
        origen =0
        for x in self.detalles.all():
            origen += x.cantidad_origen_local

        return origen 
    
    def __str__(self):
        return self.folio
    
    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        q = vars.get("q", "").strip()
        desde = vars.get("desde")
        hasta = vars.get("hasta")
        casco = vars.get("casco")
        cliente = vars.get("cliente")

        
        if desde:
            desde = to_datetime(desde, min=True)
            objects = objects.filter(fecha__gte=desde)
        if hasta:
            hasta = to_datetime(hasta, max=True)
            objects = objects.filter(fecha__lte=hasta)
        if casco:
            objects = objects.filter(casco=casco)
        if cliente:
            objects = objects.filter(cliente=cliente)

        if q:
            objects = objects.filter(models.Q(cliente__nombre__icontains=q)|models.Q(folio__icontains=q)|models.Q(recolector__nombre__icontains=q))

        objects = objects.order_by("-id")
        
        return objects
    
    def actualizar_saldo(self):

        valor = self.get_valor_cascos()
        if self.es_entrada():
            self.gran_saldo += valor
            self.saldo_cliente += valor

        else:
            self.gran_saldo -= valor
            self.saldo_cliente -= valor


    def view_url(self):
        """
        La url de la factura en admintotal.
        """
        if self.id_admintotal is None:
            return "#"

        url = settings.URL_ADMINTOTAL.replace("*", connection.tenant.schema_name)
        url += settings.VER_DETALLE_PATH
        url += f"{self.id_admintotal}/"

        return url

class MovimientoCascoDetalles(models.Model):

    movimiento = models.ForeignKey(
        MovimientoCasco, related_name="detalles", on_delete=models.PROTECT
    )
    casco = models.ForeignKey(
        Casco, related_name="casco_detalles", on_delete=models.CASCADE
    )
    cantidad_origen_local = models.IntegerField()
    cantidad_origen_pendiente = models.IntegerField()
    proveedor = models.ForeignKey(Proveedor, related_name="detalles_movimiento_cascos", null=True, blank=True, on_delete=models.PROTECT)
    enviado_proveedor = models.BooleanField(default=False)

    def get_cantidad_base1(self):
        total = self.casco.valor * self.cantidad_origen_local
        return total

    def get_monto(self):
        total = self.casco.valor * self.cantidad_origen_local

        if self.movimiento.cliente.costo_compra_casco:
            total =total*self.movimiento.cliente.costo_compra_casco
        else:
            total = total*250
        return total

    def get_total(self):
        total = self.casco.valor * self.cantidad_origen_local

        if self.movimiento.cliente.costo_compra_casco:
            total = total*self.movimiento.cliente.costo_compra_casco
        else:
            total = total*250

        return float(total)+(float(total)*0.16)

    def get_precio_venta(self):
        total = self.casco.valor * self.cantidad_origen_local

        if self.movimiento.cliente.costo_venta_casco:
            total = total*self.movimiento.cliente.costo_venta_casco
        else:
            total = total*350

        return total

    def get_precio_venta_iva(self):
        total = self.casco.valor * self.cantidad_origen_local

        if self.movimiento.cliente.costo_venta_casco:
            total = total*self.movimiento.cliente.costo_venta_casco
        else:
            total = total*350

        return float(total)+(float(total)*0.16)

class OrdenCompraDomicilio(models.Model):

    LLAMADA = 1

    COMPRA = 1

    EFECTIVO = 1
    DEBITO = 2
    CREDITO = 3

    MEDIO = (
        (LLAMADA, "Llamada"),
    )
    SERVICIO = (
        (COMPRA, "Compra"),
    )
    TIPO_PAGO = (
        (EFECTIVO, "EFECTIVO"),
        (DEBITO, "DEBITO"),
        (CREDITO, "CRIEDITO"),

    )

    numero_pedido = models.CharField(
        max_length=300,
        null=True,
        verbose_name="No. DE PEDIDO A DOMICILIO",
        unique=True,
    )
    medio = models.IntegerField(choices=MEDIO)
    fecha_de_recepcion = models.DateField(null=True)
    requiere_factura = models.BooleanField(default=False)
    datos_facturacion_tecnico =  models.BooleanField(default=False)
    cliente = models.ForeignKey(
        Cliente, 
        related_name="orden_compra_domicilio", 
        on_delete=models.CASCADE
    )
    tipo_servicio = models.IntegerField(choices=SERVICIO)
    entrega_usado = models.BooleanField(default=False)
    marca_automovil = models.CharField(
        max_length=300, 
        null=True,
        verbose_name="Marca",
    )
    modelo_automovil = models.CharField(
        max_length=300,
        null=True,
        verbose_name="Modelo"
    )
    ano_automovil = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name="Año"
    )

    #opciones

    observaciones = models.CharField(
        max_length=300,
        null=True,
    ) 
    tipo_pago = models.IntegerField(choices=TIPO_PAGO)
    requieres_cambio = models.BooleanField(default=False)
    monto_cambio = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name="Monto"
    )

    #banco = ???

    meses_sin_intereses = models.PositiveIntegerField(
        blank=True,
        null=True,
    )
    tecnico = models.ForeignKey(
        Recolector, null=True, related_name="orden_compra_domicilio", on_delete=models.CASCADE
    )
    
    #sucursal = ???

    dia = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name="Día",
    )
    hora = models.CharField(
        max_length=300,
        null=True,
    )
    agente_telefonico = models.ForeignKey(
        User, null=True, 
        related_name="orden_compra_domicilio", 
        on_delete=models.CASCADE
    )        

    calle = models.CharField(
        max_length=100, 
        blank=True, 
        null=True,
    )
    colonia = models.CharField(
        max_length=100,
        null=True,
    )
    no_i = models.CharField(
        max_length=100, 
        blank=True,
        null=True,
        verbose_name="Núm. interior"
    )
    no_e = models.CharField(
        max_length=100, 
        blank=True,
        null=True,
        verbose_name="Núm. exterior"
    )
    cp = models.CharField(
        max_length=5,
        null=True,
    )
    municipio = models.CharField(
        max_length=100, 
        blank=True,
        null=True,
    )
    estado = models.CharField(
        max_length=100, 
        blank=True,
        null=True,    
    )
    referencias = models.CharField(
        max_length=100, 
        blank=True,
        null=True,
    )
    estacionamiento = models.CharField(
        max_length=100, 
        blank=True,
        null=True,
    )

class NotaCredito(models.Model):
    folio = models.CharField(max_length=50, null=True)
    creado = models.DateTimeField(auto_now_add=True)
    vale = models.ForeignKey(MovimientoCasco, related_name="vales_nota", on_delete=models.CASCADE, null=True)
    factura = models.ManyToManyField(MovimientoCasco, related_name="facturas_nota")

    class Meta:
        ordering = ['-creado']

    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        q = vars.get("q", "").strip()
        desde = vars.get("desde")
        hasta = vars.get("hasta")

        
        if desde:
            desde = to_datetime(desde, min=True)
            objects = objects.filter(creado__gte=desde)
        if hasta:
            hasta = to_datetime(hasta, max=True)
            objects = objects.filter(creado__lte=hasta)

        if q:
            objects = objects.filter(models.Q(vale__cliente__nombre__icontains=q)|models.Q(id__icontains=q))

        
        return objects

    def get_aplicado(self):
        total =0
        for a in self.detalle_factura.all():
            total += a.cantidad
        
        return total


class PagoFactura(models.Model):
    creado= models.DateTimeField(auto_now_add=True)
    nota = models.ForeignKey(NotaCredito, related_name="detalle_factura", on_delete=models.CASCADE, null=True)
    factura = models.ForeignKey(MovimientoCasco, related_name="detalle_nota", on_delete=models.CASCADE, null=True)
    cantidad = models.FloatField()

    def get_importe_pago(self):
        cliente = self.factura.cliente
        total = self.cantidad
        if cliente.costo_compra_casco:
            total = total*self.cliente.costo_compra_casco
        else:
            total = total*220

        return total













        



