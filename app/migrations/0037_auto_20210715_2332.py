# Generated by Django 3.0.5 on 2021-07-15 23:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0036_auto_20210701_1826'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='codigo',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='cp',
            field=models.CharField(max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='domicilio',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='cliente',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='estado',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='localidad',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='municipio',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='cliente',
            name='no_e',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='cliente',
            name='no_i',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='cliente',
            name='pais',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='referencia',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='rfc',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
