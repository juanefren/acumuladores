# Generated by Django 3.0.5 on 2020-11-26 21:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20201126_2110'),
    ]

    operations = [
        migrations.RenameField(
            model_name='movimientocasco',
            old_name='saldo',
            new_name='gran_saldo',
        ),
        migrations.AddField(
            model_name='movimientocasco',
            name='saldo_cliente',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=12),
        ),
    ]
