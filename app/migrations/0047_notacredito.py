# Generated by Django 3.0.5 on 2021-09-04 17:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0046_movimientocasco_url_pdf'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotaCredito',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('folio', models.CharField(max_length=255)),
                ('factura', models.ManyToManyField(related_name='facturas_nota', to='app.MovimientoCasco')),
                ('vale', models.ManyToManyField(related_name='vales_nota', to='app.MovimientoCasco')),
            ],
        ),
    ]
