# Generated by Django 3.0.5 on 2021-08-26 21:39

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0039_recolector_id_admintotal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimientocasco',
            name='fecha',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha de recolección'),
        ),
    ]
