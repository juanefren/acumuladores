# Generated by Django 3.0.5 on 2021-05-03 21:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0028_recolector'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Recolector',
            new_name='Recolectores',
        ),
    ]
