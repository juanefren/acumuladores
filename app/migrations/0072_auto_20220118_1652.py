# Generated by Django 3.0.5 on 2022-01-18 16:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0071_auto_20220118_1625'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimientocasco',
            name='proveedor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='movimiento_cascos', to='app.Proveedor'),
        ),
        migrations.AlterField(
            model_name='movimientocasco',
            name='cliente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='movimiento', to='app.Cliente'),
        ),
        migrations.AlterField(
            model_name='movimientocascodetalles',
            name='proveedor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='detalles_movimiento_cascos', to='app.Proveedor'),
        ),
    ]
