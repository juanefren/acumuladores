# Generated by Django 3.0.5 on 2020-10-22 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20201022_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimientocasco',
            name='tipo',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Entrada'), (2, 'Salida')], default=1),
            preserve_default=False,
        ),
    ]
