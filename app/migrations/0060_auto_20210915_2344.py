# Generated by Django 3.0.5 on 2021-09-15 23:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0059_auto_20210913_2159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimientocasco',
            name='cantidad_origen',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
