# Generated by Django 3.0.5 on 2021-05-10 17:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0032_auto_20210504_1955'),
    ]

    operations = [
        migrations.CreateModel(
            name='MovimientoCascoDetalles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad_origen_local', models.IntegerField()),
                ('casco', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='casco_detalles', to='app.Casco')),
                ('movimiento', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='detalles', to='app.MovimientoCasco')),
            ],
        ),
    ]
