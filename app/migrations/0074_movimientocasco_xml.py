# Generated by Django 3.0.5 on 2022-01-20 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0073_movimientocasco_salida_proveedor'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimientocasco',
            name='xml',
            field=models.TextField(blank=True),
        ),
    ]
