# Generated by Django 3.0.5 on 2021-09-09 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0054_remove_notacredito_folio'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimientocasco',
            name='cantidad_origen',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
