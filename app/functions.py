from urllib import request
from django.conf import settings
from configuracion.models import Configuracion
from .models import MovimientoCasco, Cliente, Casco, Proveedor, Recolector, MovimientoCascoDetalles, TotalesMovimientosMeses, Almacen
from django.db import connection
import requests
from decimal import Decimal
from tools.functions import to_datetime
import datetime
import json

def regenerar_existencias_kardex(desde):
    """
    Regenera el saldo (existencia) total y por cliente de los movimientos 
    según las entradas/salidas 
    """    
    configuracion = Configuracion.objects.get()
    if desde is None or desde < to_datetime(configuracion.fecha_inicio):
        desde = configuracion.fecha_inicio


    gran_saldo = None
    saldos_cliente = {}
    ajustes_cliente_aplicados = []

    ultimo_movimiento_cliente = None

    movs = MovimientoCasco.objects.filter(fecha__gte=desde)
    for m in movs.order_by("fecha", "id").prefetch_related("casco", "cliente"):

        if gran_saldo is None:
            ultimo_movimiento = MovimientoCasco.objects.filter(
                fecha__lt=desde,
            ).order_by("-fecha", "-id").first()

            if ultimo_movimiento is None:
                gran_saldo = configuracion.saldo_inicial
            else:
                gran_saldo = ultimo_movimiento.gran_saldo

        

        #if saldos_cliente.get(m.cliente_id) is None:
        #    saldos_cliente[m.cliente_id] = 0


        if m.cliente.fecha_saldo_inicial and \
            m.cliente.fecha_saldo_inicial >= m.fecha and \
            not m.cliente_id in ajustes_cliente_aplicados:

            saldos_cliente[m.cliente_id] = m.cliente.saldo_inicial
            ajustes_cliente_aplicados.append(m.cliente)
        
        elif saldos_cliente.get(m.cliente_id) is None:

            ultimo_movimiento_cliente = MovimientoCasco.objects.filter(
                fecha__lt=desde,
                cliente=m.cliente,
            ).order_by("-fecha", "-id").first()
            
            if ultimo_movimiento_cliente:
                saldos_cliente[m.cliente_id] = ultimo_movimiento.saldo_cliente
            else:
                saldos_cliente[m.cliente_id] = 0

        
        valor = m.get_valor_cascos()

        if not m.cliente.es_proveedor:
            valor = valor * -1

        if m.tipo == MovimientoCasco.TM_ENTRADA:
            gran_saldo += valor
            saldos_cliente[m.cliente_id] += valor

        elif m.tipo == MovimientoCasco.TM_SALIDA:
            gran_saldo -= valor
            saldos_cliente[m.cliente_id] -= valor


        m.gran_saldo = gran_saldo
        if m.cliente.fecha_saldo_inicial and m.cliente.fecha_saldo_inicial < m.fecha:
            m.saldo_cliente = 0
        else:    
            m.saldo_cliente = saldos_cliente[m.cliente_id]
        m.save(update_fields=["gran_saldo", "saldo_cliente"])

    return None






def sincronizar_kardex_admintotal(desde):


    instance = Configuracion.objects.first()
    headers = {
        "api-key": instance.api_key,
        "content-type": "application/json",
    }

    #API Tomar recolectores de admintotal

    url_api_recolectores = settings.URL_ADMINTOTAL+"/api/v2/vendedores_externos/"
    url_rec = url_api_recolectores.replace("*", connection.tenant.schema_name)

    u_r = requests.get(url_rec, headers=headers)
    if u_r.status_code != 200:
        return f"Hubo un errror al conectarse con Admintotal status_code: {u_r.status_code} respuesta: {u_r.text}"

    u_data = u_r.json()

    for d in u_data["results"]:
        try:
            recolector = Recolector.objects.get(id_admintotal=d["id"])
        except Recolector.DoesNotExist:
            recolector = Recolector.objects.create(
                nombre=d["nombre"]+" "+d["apellido_paterno"]+" "+d["apellido_materno"], id_admintotal=d["id"])
            recolector.save()




    """
    API REPARTIDORES
    """

    url_api_recolectores = settings.URL_ADMINTOTAL+"/api/v2/repartidores/"
    url_rec = url_api_recolectores.replace("*", connection.tenant.schema_name)
   
    u_r = requests.get(url_rec, headers=headers)
    
    if u_r.status_code != 200:
        return f"Hubo un errror al conectarse con Admintotal status_code: {u_r.status_code} respuesta: {u_r.text}"

    u_data = u_r.json()
    for d in u_data["results"]:
        try:
            recolector = Recolector.objects.get(id_admintotal=d["id"])
        except Recolector.DoesNotExist:
            recolector = Recolector.objects.create(
                nombre=d["nombre"]+" "+d["apellido_paterno"]+" "+d["apellido_materno"], id_admintotal=d["id"])
            recolector.save()
    """
    Se comunica con Admintotal para descargar los movimiento, en caso de haber
    algún error se regresa en la función
    """
    
    

    url_api_cascos = settings.URL_ADMINTOTAL + settings.PATH_API_CASCOS
    url_api_recolectores = settings.URL_ADMINTOTAL + settings.PATH_API_CASCOS
    url = url_api_cascos.replace("*", connection.tenant.schema_name)
    if desde:
        url += f"?desde={desde}"

    r = requests.get(url, headers=headers)
    
    if r.status_code != 200:
        return f"Hubo un errror al conectarse con Admintotal status_code: {r.status_code} respuesta: {r.text}"
        
    cascos = r.json()

    for c in cascos:
        d = datetime.datetime.fromisoformat(c["fecha"][:-1]).date()

        


        try:
            casco = Casco.objects.get(codigo=c["producto"])
        except Casco.DoesNotExist:
            #return f'No existe el casco con código {c["producto"]}'
            casco = Casco.objects.create(codigo=c["producto"], valor=0)

        if TotalesMovimientosMeses.objects.filter(date_mes=d.strftime("%b-%Y").upper()).exists():
            total_obj=TotalesMovimientosMeses.objects.get(date_mes=d.strftime("%b-%Y").upper())
            total_obj.cantidad_salida += float(Decimal(c["cantidad"])*casco.valor)
            total_obj.save()

        else:
            total_obj=TotalesMovimientosMeses.objects.create(cantidad_salida=(Decimal(c["cantidad"])*casco.valor), date_mes=d.strftime("%b-%Y").upper())

        proveedor = None
        if "proveedor" in c:
            if c["proveedor"]:

                try:
                    proveedor = Proveedor.objects.get(
                        id_admintotal=c["proveedor"]["id"])
                except Proveedor.DoesNotExist:
                    proveedor = Proveedor.objects.create(
                        nombre=c["proveedor"]["nombre"], id_admintotal=c["proveedor"]["id"]).save()

               

        if c["recolector"]:
            try:
                recolector = Recolector.objects.get(id_admintotal=c["recolector"]["id"])
            except Recolector.DoesNotExist:
                recolector = Recolector.objects.create(nombre=c["recolector"]["nombre"], id_admintotal=c["recolector"]["id"] ).save()
        else:
            recolector = None
        if c["cliente"]:
            try:
                cliente = Cliente.objects.get(id_admintotal=c["cliente"]["id"])
            except Cliente.DoesNotExist:
                cliente = Cliente.objects.create(
                    id_admintotal=c["cliente"]["id"],
                    nombre=c["cliente"]["nombre"],
                )
        else:
            cliente = Cliente.objects.get_or_create(
                nombre="CLIENTE NO ESPECIFICADO EN ADMINTOTAL",
            )[0]
        if MovimientoCasco.objects.filter(id_admintotal=c["id"]).exists():
            movimiento_casco = MovimientoCasco.objects.get(id_admintotal=c["id"])
            
            try:
                if movimiento_casco.cancelado == False and c["cancelado"]:
                    movimiento_casco = MovimientoCasco(id_admintotal=c["id"])

                    
                    movimiento_casco.casco = casco
                    movimiento_casco.cliente = cliente
                    movimiento_casco.fecha = d
                    movimiento_casco.cantidad_origen += (Decimal(c["cantidad"])*casco.valor)
                    movimiento_casco.cantidad_restante += (Decimal(c["cantidad"])*casco.valor)
                    movimiento_casco.recolector = recolector
                    movimiento_casco.fecha_year_mes = total_obj
                    movimiento_casco.cancelado = c["cancelado"]
                    movimiento_casco.es_vale = False
                    if c["es_entrada"]:
                        movimiento_casco.tipo = 1
                    elif c["es_salida"]:
                        movimiento_casco.tipo = 2
                    else:
                        raise ValueError("El movimiento debe ser entrada o salida")
                    
                    movimiento_casco.save()
            except:
                continue
            

        else:
            movimiento_casco = MovimientoCasco(id_admintotal=c["id"])

            movimiento_casco.casco = casco
            movimiento_casco.cliente = cliente
            movimiento_casco.fecha = d
            movimiento_casco.cantidad_origen = (Decimal(c["cantidad"])*casco.valor)
            movimiento_casco.cantidad_restante = (Decimal(c["cantidad"])*casco.valor)
            movimiento_casco.fecha_year_mes = total_obj
            movimiento_casco.folio = c["folio"]
            movimiento_casco.recolector = recolector
            movimiento_casco.url_pdf = settings.URL_ADMINTOTAL.replace("*", connection.tenant.schema_name) + c["url"]
            movimiento_casco.es_vale = False
            #movimiento_casco.cancelado = c["cancelado"]
            if c["es_entrada"]:
                movimiento_casco.tipo = 1
            elif c["es_salida"]:
                movimiento_casco.tipo = 2
            else:
                raise ValueError("El movimiento debe ser entrada o salida")
            
            movimiento_casco.save()

        try:
            MovimientoCascoDetalles.objects.get(movimiento=movimiento_casco, casco=casco, proveedor=proveedor)
        except MovimientoCascoDetalles.DoesNotExist:
            MovimientoCascoDetalles.objects.create(
                movimiento=movimiento_casco, casco=casco, cantidad_origen_local=c["cantidad"], cantidad_origen_pendiente=c["cantidad"], proveedor=proveedor)



        


    
    #API Tomar las garatias de los admintotal

    url_garantia = settings.URL_ADMINTOTAL+"/api/v2/cascos_garatias/"
    url_garantia = url_garantia.replace("*", connection.tenant.schema_name)

    if desde:
        url_garantia += f"?desde={desde}"

    request_garantia = requests.get(url_garantia, headers=headers)
    if request_garantia.status_code != 200:
        return f"Hubo un errror al conectarse con Admintotal status_code: {request_garantia.status_code} respuesta: {request_garantia.text}"

    garantia = request_garantia.json()

    

    for c in garantia:
        d = datetime.datetime.fromisoformat(c["fecha"][:-1]).date()

        try:
            casco = Casco.objects.get(codigo=c["producto"])
        except Casco.DoesNotExist:
            #return f'No existe el casco con código {c["producto"]}'
            casco = Casco.objects.create(codigo=c["producto"], valor=0)

        if TotalesMovimientosMeses.objects.filter(date_mes=d.strftime("%b-%Y").upper()).exists():
            total_obj = TotalesMovimientosMeses.objects.get(
                date_mes=d.strftime("%b-%Y").upper())
            total_obj.cantidad_salida += float(
                Decimal(c["cantidad"])*casco.valor)
            total_obj.save()

        else:
            total_obj = TotalesMovimientosMeses.objects.create(cantidad_salida=(
                Decimal(c["cantidad"])*casco.valor), date_mes=d.strftime("%b-%Y").upper())

        if c["recolector"]:
            try:
                recolector = Recolector.objects.get(
                    id_admintotal=c["recolector"]["id"])
            except Recolector.DoesNotExist:
                recolector = Recolector.objects.create(
                    nombre=c["recolector"]["nombre"], id_admintotal=c["recolector"]["id"]).save()
        else:
            recolector = None
        
        if c["almacen"]:
            
            try:
                almacen = Almacen.objects.get(
                    id_admintotal=c["almacen"]["id"])
            except Almacen.DoesNotExist:
                almacen = Almacen.objects.create(
                    nombre=c["almacen"]["nombre"], id_admintotal=c["almacen"]["id"]).save()
        else:
            almacen = None

        proveedor = None
        if "proveedor" in c:
            if c["proveedor"]:

                try:
                    proveedor = Proveedor.objects.get(
                        id_admintotal=c["proveedor"]["id"])
                except Proveedor.DoesNotExist:
                    proveedor = Proveedor.objects.create(
                        nombre=c["proveedor"]["nombre"], id_admintotal=c["proveedor"]["id"]).save()                
        if c["cliente"]:
            try:
                cliente = Cliente.objects.get(id_admintotal=c["cliente"]["id"])
            except Cliente.DoesNotExist:
                cliente = Cliente.objects.create(
                    id_admintotal=c["cliente"]["id"],
                    nombre=c["cliente"]["nombre"],
                )
        else:
            cliente = Cliente.objects.get_or_create(
                nombre="CLIENTE NO ESPECIFICADO EN ADMINTOTAL",
            )[0]
        if MovimientoCasco.objects.filter(id_admintotal=c["id"]).exists():
            movimiento_casco = MovimientoCasco.objects.get(
                id_admintotal=c["id"])

            try:
                if movimiento_casco.cancelado == False and c["cancelado"]:
                    movimiento_casco = MovimientoCasco(id_admintotal=c["id"])

                    movimiento_casco.casco = casco
                    movimiento_casco.cliente = cliente
                    movimiento_casco.fecha = d
                    movimiento_casco.cantidad_origen += (
                        Decimal(c["cantidad"])*casco.valor)
                    movimiento_casco.cantidad_restante += (
                        Decimal(c["cantidad"])*casco.valor)
                    movimiento_casco.recolector = recolector
                    movimiento_casco.fecha_year_mes = total_obj
                    movimiento_casco.cancelado = c["cancelado"]
                    movimiento_casco.es_garantia = True
                    movimiento_casco.almacen = almacen
                    movimiento_casco.es_vale = False
                    if c["es_entrada"]:
                        movimiento_casco.tipo = 1
                    elif c["es_salida"]:
                        movimiento_casco.tipo = 2
                    else:
                        raise ValueError(
                            "El movimiento debe ser entrada o salida")

                    movimiento_casco.save()
            except:
                continue

        else:
            movimiento_casco = MovimientoCasco(id_admintotal=c["id"])

            movimiento_casco.casco = casco
            movimiento_casco.cliente = cliente
            movimiento_casco.fecha = d
            movimiento_casco.cantidad_origen = (
                Decimal(c["cantidad"])*casco.valor)
            movimiento_casco.cantidad_restante = (
                Decimal(c["cantidad"])*casco.valor)
            movimiento_casco.fecha_year_mes = total_obj
            movimiento_casco.folio = c["folio"]
            movimiento_casco.recolector = recolector
            movimiento_casco.url_pdf = settings.URL_ADMINTOTAL.replace(
                "*", connection.tenant.schema_name) + c["url"]
            movimiento_casco.es_vale = False
            movimiento_casco.es_garantia = True
            movimiento_casco.almacen = almacen
            if c["es_entrada"]:
                movimiento_casco.tipo = 1
            elif c["es_salida"]:
                movimiento_casco.tipo = 2
            else:
                raise ValueError("El movimiento debe ser entrada o salida")

            movimiento_casco.save()

        try:
            MovimientoCascoDetalles.objects.get(movimiento=movimiento_casco, casco=casco, proveedor=proveedor)
        except MovimientoCascoDetalles.DoesNotExist:
            MovimientoCascoDetalles.objects.create(
                movimiento=movimiento_casco, casco=casco, cantidad_origen_local=c["cantidad"], cantidad_origen_pendiente=c["cantidad"], proveedor=proveedor)



def get_years():
    resultado = []
    obj_first = MovimientoCasco.objects.all().first()
    obj_last = MovimientoCasco.objects.all().last()

    if obj_first and obj_last:
        first_year = obj_first.creado.strftime('%Y')
        last_year = obj_last.creado.strftime('%Y')

        resultado.append((first_year,first_year))


        if last_year != first_year:
            resultado.append((last_year,last_year))

    return resultado


def EnviarEntradaUsado(casco,cantidad,folio, precio_compra):
    instance = Configuracion.objects.first()

    url_tmp = settings.URL_ADMINTOTAL + "/api/v2/casco_venta/"
    url = url_tmp.replace("*", connection.tenant.schema_name)

    


    data = {
        "casco_nombre":casco,
        "cantidad":cantidad,
        "folio":folio,
        "precio_compra": precio_compra,
    }
    headers = {
        "api-key": instance.api_key,
        "content-type": "application/json",
    }

    r = requests.post(url, headers=headers, data=json.dumps(data))

    if r.status_code != 201:
        return f"Hubo un errror al conectarse con Admintotal status_code: {r.status_code} respuesta: {r.text}"
    else:
        return 200


def  EnviarNotaCredito(data):
    instance = Configuracion.objects.first()

    url_tmp = settings.URL_ADMINTOTAL + "/api/v2/movimientos/notas_credito/"
    url = url_tmp.replace("*", connection.tenant.schema_name)

    
    headers = {
        "api-key": instance.api_key,
        "content-type": "application/json",
    }

    r = requests.post(url, headers=headers, data=json.dumps(data))

    if r.status_code != 201:
        return f"Hubo un errror al conectarse con Admintotal status_code: {r.status_code} respuesta: {r.text}"
    else:
        return 200


def to_int(s):
    try:
        return int(s)
    except:
        return 0
