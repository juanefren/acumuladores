from tools.classes import ExcelFile
from tools.functions import (
    safe_division as div, 
)

def catalogo_clientes(objects):

    hoja = ExcelFile(nombre_hoja="Recursos")

    hoja.write_title("ID admintotal")
    hoja.write_title("Nombre")
    
    for u in objects:
        hoja.add_row()
        hoja.write_str(u.id_admintotal)
        hoja.write_str(u.nombre)

    return hoja.xlsx_to_response("clientes.xlsx")