from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from django.forms import modelformset_factory, formset_factory
from django.db.models import Sum, F, FloatField, ExpressionWrapper, Q
from decimal import Decimal
from django.db import models
from tools.functions import to_datetime

from .forms import (
    CascoForm,
    ClienteForm,
    MovimientoCascoForm,
    FiltroForm,
    FechaForm,
    FiltroClientesForm,
    MovimientoCascoDetallesFS,
    OrdenCompraDomicilioForm,
    DetallesSalidaProveedorFS,
    SalidaProveedorForm,
)

from .functions import (
    sincronizar_kardex_admintotal,EnviarEntradaUsado,
    regenerar_existencias_kardex as fn_regenerar_existencias_kardex,
    EnviarNotaCredito, to_int

)

from .models import (
    Casco,
    MovimientoCasco,
    Cliente,
    MovimientoCascoDetalles,
    OrdenCompraDomicilio,
    NotaCredito,
    PagoFactura,
    Proveedor,
    TotalesMovimientosMeses,
    InventarioAlmacen,
)

from . import exportar_excel

from tools.functions import (
    error_response, json_response, list_view, render_pdf
)

from django.conf import settings
from django.utils import timezone
from configuracion.models import Configuracion

import json
import datetime



@login_required
def index(request):
    """
    Pantalla inicial
    """
    return render(request, "app/dashboard.html", {"title": ""})


@login_required
def sincronizar_kardex(request):
    """
    Guarda los datos de la api(Control Cascos) en el models.
    """

    if request.method == "POST":
        form = FechaForm(request.POST)
        if not form.is_valid():
            return error_response(request, str(form.errors))

        desde = form.cleaned_data.get("fecha")
        #if not desde:
        #    return error_response(request, f"No se recibió correctamente la fecha desde: {desde}")

        error = sincronizar_kardex_admintotal(desde)
        if error:
            return error_response(request, error)

        messages.success(request, "Se ha sincronizado el kárdex con la información de Admintotal")
        return redirect("app:kardex")
    else:
        form = FechaForm()
    
    
    return render(
        request, 
        "app/sincronizar_kardex.html", 
        {
            "title": "Sincronizar kárdex con Admintotal",
            "form":form,
        },
    )


@login_required
def regenerar_existencias_kardex(request):
    """
    Regera las existencias de los cascos.
    """

    if request.method == "POST":
        form = FechaForm(request.POST)
        if not form.is_valid():
            return error_response(request, str(form.errors))

        desde = form.cleaned_data.get("fecha")
        #if not desde:
        #    return error_response(request, f"No se recibió correctamente la fecha desde: {desde}")

        error = fn_regenerar_existencias_kardex(desde)
        if error:
            return error_response(request, error)

        messages.success(request, "Se ha regenerado el kárdex")
        return redirect("app:kardex")
    else:
        form = FechaForm()
    
    
    return render(
        request, 
        "app/regenerar_kardex.html", 
        {
            "title": "Sincronizar kárdex con Admintotal",
            "form":form,
        },
    )

@login_required
def contador_usados(request):
    """
    Pantalla para mostrar los datos de la API. Antes se llamaba Kardex y 
    se cambió el nombre
    """
    
    rget = request.GET.copy()
    hoy = timezone.now()
    filtro_form = FiltroForm(rget, usuario=request.user)

    if not filtro_form.is_valid():
        return error_response(request, filtro_form.errors)
    
    rget["desde"] = filtro_form.cleaned_data["desde"]
    rget["hasta"] = filtro_form.cleaned_data["hasta"]
    rget["q"] = filtro_form.cleaned_data["q"]


    cascos = MovimientoCasco.get_objects(rget).order_by(
        "-fecha", "-id").prefetch_related("cliente", "casco")

    saldo_global = 0
    objs  = MovimientoCasco.get_objects(rget).order_by("cliente","-fecha", "-id").distinct('cliente')
    cascos_obj = Casco.objects.all()

    total_por_casco=[]
    for a in objs:
        hg = []
        total_por_cliente=0
        for index,c in enumerate(cascos_obj):
            suma=0
            obj_by_cliente=MovimientoCascoDetalles.objects.filter(movimiento__cliente= a.cliente, casco=c)
            if rget["desde"]:
                obj_by_cliente=obj_by_cliente.filter(movimiento__fecha__gte=rget["desde"])
            if rget["hasta"]:
                obj_by_cliente=obj_by_cliente.filter(movimiento__fecha__lte=rget["hasta"])
            for x in obj_by_cliente:
                
                if x.movimiento.es_entrada():
                    suma += int(x.cantidad_origen_local)
                    total_por_cliente += float(x.get_cantidad_base1())
                elif x.movimiento.es_salida():
                    suma -= int(x.cantidad_origen_local)
                    total_por_cliente -= float(x.get_cantidad_base1())
            hg.insert(index,suma)
        a.suma_por_cliente = total_por_cliente
        a.sums_cascos = hg


    return list_view(
        request,
        objs,
        template="app/contador_usados.html",
        context={
            "title": "Contador de Usados",
            "NODO_BREADCRUMB": "acumuladores:contador_usados",
            "exportar_excel": True,
            "filtro_form":filtro_form,
            "no_mostrar_buscar":True,
            "hoy":hoy,
            "saldo_global": saldo_global,
            "cascos": cascos_obj,
            
        },
    )

def kardex_usados(request):
    return kardex(request,es_garantia=False)


def kardex_garantia(request):
    return kardex(request, es_garantia=True)

@login_required
def kardex(request, es_garantia):
    """
    Pantalla para mostrar los datos de la API. Antes se llamaba Kardex y 
    se cambió el nombre
    """
    
    rget = request.GET.copy()
    hoy = timezone.now()
    filtro_form = FiltroForm(rget, usuario=request.user)

    if not filtro_form.is_valid():
        return error_response(request, filtro_form.errors)
    
    rget["desde"] = filtro_form.cleaned_data["desde"]
    rget["hasta"] = filtro_form.cleaned_data["hasta"]
    rget["casco"] = filtro_form.cleaned_data["casco"]
    rget["cliente"] = filtro_form.cleaned_data["cliente"]



    saldo_global = 0
    objs  = MovimientoCasco.get_objects(rget).filter(es_garantia=es_garantia, tipo__in=[1,2]).order_by("folio","-fecha", "-id").distinct('folio')
    cascos_obj = Casco.objects.all()
    total_base1 = []
    total_por_casco=[]
    for a in objs:
        a.total_base1=0
        hg = []
        for index,c in enumerate(cascos_obj):
            obj_by_cliente=MovimientoCascoDetalles.objects.filter(movimiento__folio= a.folio, casco=c)
            if rget["desde"]:
                obj_by_cliente = obj_by_cliente.filter(movimiento__fecha__gte=rget["desde"])
            if rget["hasta"]:
                obj_by_cliente = obj_by_cliente.filter(movimiento__fecha__lte=rget["hasta"])
            
            obj_by_cliente = obj_by_cliente.aggregate(Sum('cantidad_origen_local'))
            total = int(obj_by_cliente['cantidad_origen_local__sum']) if obj_by_cliente['cantidad_origen_local__sum'] else 0
            
            a.total_base1 = a.total_base1+((int(obj_by_cliente['cantidad_origen_local__sum']) if obj_by_cliente['cantidad_origen_local__sum'] else 0)*c.valor)
            hg.insert(index,total)
            if len(total_por_casco)>=len(cascos_obj):
                total_por_casco[index]=total_por_casco[index]+total
            else:
                total_por_casco.insert(index, total)
        a.sums_cascos = hg
        
        

    return list_view(
        request,
        objs,
        template="app/kardex.html",
        context={
            "es_garantia":es_garantia,
            "title": "Contador de Usados",
            "NODO_BREADCRUMB": "acumuladores:contador_usados",
            "exportar_excel": True,
            "filtro_form":filtro_form,
            "no_mostrar_buscar":True,
            "hoy":hoy,
            "saldo_global": saldo_global,
            "cascos": cascos_obj,
            "total_por_casco":total_por_casco,
            "total_base1": total_base1,
            "es_garantia":es_garantia,
            
        },
    )



def salidas_proveedor(request):
    get = request.GET.copy()
    title = "Salidas de usados a proveedor"
    form = FiltroForm(get, usuario=request.user)

    if not form.is_valid():
        return error_response(request, form.errors)


    get["desde"] = form.cleaned_data["desde"]
    get["hasta"] = form.cleaned_data["hasta"]
    get["casco"] = form.cleaned_data["casco"]
    get["q"] = form.cleaned_data["q"]

    objs = MovimientoCasco.get_objects(get).filter(tipo__in =[3,4])


    return list_view(
        request,
        objs,
        template="app/salidas_proveedor.html",
        context={
            "title": title,
            "filtro_form": form,
            "no_mostrar_buscar": True,
        }
        )



def contador_cliente(request,id):
    """
    Se cambió el nombre de kardex cliente a contador cliente
    """
    
    get = request.GET.copy()
    hoy = timezone.now()
    form = FiltroForm(get, usuario=request.user)

    if not form.is_valid():
        return error_response(request, form.errors)

    saldo_global = 0
    get["desde"] = form.cleaned_data["desde"]
    get["hasta"] = form.cleaned_data["hasta"]
    get["casco"] = form.cleaned_data["casco"]
    get["q"] = form.cleaned_data["q"]
    get["cliente"] = id
    

    saldo_global = 0
    objs  = MovimientoCasco.get_objects(get).order_by("folio","-fecha", "-id").distinct('folio')
    cascos_obj = Casco.objects.all()

    total_por_vale=[]
    for a in objs:
        hg = []
        total_por_vale=0
        for index,c in enumerate(cascos_obj):
            suma=0
            obj_by_cliente=MovimientoCascoDetalles.objects.filter(movimiento__folio= a.folio, casco=c)
            if get["desde"]:
                obj_by_cliente=obj_by_cliente.filter(movimiento__fecha__gte=get["desde"])
            if get["hasta"]:
                obj_by_cliente=obj_by_cliente.filter(movimiento__fecha__lte=get["hasta"])
            for x in obj_by_cliente:
                if x.movimiento.es_entrada():
                    suma += int(x.cantidad_origen_local)
                    total_por_vale += float(x.get_cantidad_base1())
                elif x.movimiento.es_salida():
                    suma -= int(x.cantidad_origen_local)
                    total_por_vale -= float(x.get_cantidad_base1())
            hg.insert(index,suma)
        a.total_por_vale = total_por_vale
        a.sums_cascos = hg

    return list_view(
        request,
        objs,
        template="app/contador_cliente.html",
        context={
            "title": "Contador cliente",
            "NODO_BREADCRUMB": "acumuladores:contador_usados",
            "exportar_excel": True,
            "filtro_form":form,
            "hoy":hoy,
            "saldo_global": saldo_global,
            "cascos": cascos_obj,
            "total_por_vale":total_por_vale
            
        },
    )

def graficas_movimientos(request):

    rget = request.GET.copy()
    total_casusa1=0
    total_casusa2=0
    total_casusa3=0
    total_casusa4=0
    total_casusa5=0
    total_casusa6=0
    total_casusa7=0
    no_mostrar_elemento=True
    no_mostrar_buscar=True

    filtro_form = FiltroForm(rget, usuario=request.user)
    if not filtro_form.is_valid():
        return error_response(request, filtro_form.errors)

    rget["desde"] = filtro_form.cleaned_data["desde"]
    rget["hasta"] = filtro_form.cleaned_data["hasta"]
    rget["casco"] = filtro_form.cleaned_data["casco"]

    cascos = MovimientoCasco.get_objects(rget).order_by(
        "-fecha", "-id").prefetch_related("cliente", "casco")

    for c in cascos.filter(casco__codigo='CASUSA1'):
        if c.tipo == 2:
            total_casusa1 += c.get_cantidad_origen()

    for c in cascos.filter(casco__codigo='CASUSA2'):
        if c.tipo == 2:
            total_casusa2 += c.get_cantidad_origen()
    
    for c in cascos.filter(casco__codigo='CASUSA3'):
        if c.tipo == 2:
            total_casusa3 += c.get_cantidad_origen()
    
    for c in cascos.filter(casco__codigo='CASUSA4'):
        if c.tipo == 2:
            total_casusa4 += c.get_cantidad_origen()
    
    for c in cascos.filter(casco__codigo='CASUSA5'):
        if c.tipo == 2:
            total_casusa5 += c.get_cantidad_origen()

    for c in cascos.filter(casco__codigo='CASUSA6'):
        if c.tipo == 2:
            total_casusa6 += c.get_cantidad_origen()
    
    for c in cascos.filter(casco__codigo='CASUSA7'):
        if c.tipo == 2:
            total_casusa7 += c.get_cantidad_origen()  
            

    return list_view(
        request,
        cascos,
        template="app/graficas_movimientos.html",
        context={
            "title": "Graficas de movimientos",
            "filtro_form":filtro_form,
            "total_casusa1": total_casusa1,
            "total_casusa2": total_casusa2,
            "total_casusa3": total_casusa3,
            "total_casusa4": total_casusa4,
            "total_casusa5": total_casusa5,
            "total_casusa6": total_casusa6,
            "total_casusa7": total_casusa7,
            "no_mostrar_elemento": no_mostrar_elemento,
            "no_mostrar_buscar": no_mostrar_buscar,
        },
    )

@login_required
def kardex_entradas_almacen(request):
    """
    Pantalla para mostrar los datos local.
    """
    rget = request.GET.copy()
    hoy = timezone.now()
    filtro_form = FiltroForm(rget, usuario=request.user)

    if not filtro_form.is_valid():
        return error_response(request, filtro_form.errors)
    
    rget["desde"] = filtro_form.cleaned_data["desde"]
    rget["hasta"] = filtro_form.cleaned_data["hasta"]

    cascos = MovimientoCasco.get_objects(rget).order_by(
        "-fecha", "-id").prefetch_related("cliente", "casco").filter(
            id_admintotal__isnull=True, tipo=1)
   
    return list_view(
        request,
        cascos,
        template="app/kardex_entradas_almacen.html",
        context={
            "title": "Kárdex en almacén",
            "NODO_BREADCRUMB": "acumuladores:kardex_entradas_almacen",
            "exportar_excel": True,
            "filtro_form":filtro_form,
            "no_mostrar_buscar":True,
            "hoy":hoy,
            
        },
    )



def agregar_salida_usado_proveedor(request):
    return agregar_salida_proveedor(request, tipo=3)


def agregar_salida_garantia_proveedor(request):
    return agregar_salida_proveedor(request, tipo=4)


def agregar_salida_proveedor(request, tipo=3):
    cascos = Casco.objects.all()
    instance = MovimientoCasco()

    m = MovimientoCasco.objects.filter(tipo__in=[3,4]).count()
    folio = f"SP-{m}"

    if tipo == 3:
        title = "Agregar salida a proveedor"
        ir = "app:salidas_proveedor"
    else: 
        title = "Agregar salida a proveedor por garantia"
        ir = "app:salidas_proveedor_garantias"


    if request.method == "POST":
        form = SalidaProveedorForm(request.POST,instance=instance)

        if form.is_valid():
            formset = DetallesSalidaProveedorFS(request.POST, form_kwargs={"proveedor": form.cleaned_data["proveedor"]})
            if formset.is_valid():
                form.instance.tipo = tipo
                form.instance.usuario_creado = request.user
                form.save()
                for fs in formset.forms:
                    if fs.is_valid():
                        f_folio = fs.cleaned_data["folio"]
                        m = MovimientoCasco.objects.filter(folio=f_folio).first()
                        m.salida_proveedor = form.instance
                        m.save()
                messages.success(
                request, "Los datos han sido guardados correctamente."
                )
                return redirect(ir)

    else:
        form = SalidaProveedorForm(instance=instance)
        formset = DetallesSalidaProveedorFS(form_kwargs={"proveedor": 0})


    return render(
        request, 
        "app/agregar_salida_proveedor.html", 
        {
            "title": title,
            "form": form,
            "formset":formset,
            "cascos": cascos,
            "folio":folio,
            "tipo": tipo,
        }
    )


def agregar_salida(request):
    return agregar_movimiento(request, tipo=2)


def agregar_entrada(request):
    return agregar_movimiento(request, tipo=1)

def agregar_movimiento(request, tipo):
    """
    Pantalla para agregar un casco 
    """
    instance = MovimientoCasco()
    form = MovimientoCascoForm(instance=instance)
    cascos = Casco.objects.all()

    if request.method == "POST":
        
        form = MovimientoCascoForm(
            request.POST, instance=instance, usuario=request.user
        )
        
        if form.is_valid():
            total=0
            form.save(commit=False)
            cliente = Cliente.objects.filter(nombre=form.cleaned_data["cliente_ac"]).first()
            fecha= form.cleaned_data["fecha"]
            folio = form.cleaned_data["folio"]
            
            form.instance.cliente = cliente
            form.instance.recolector = form.cleaned_data["recolector"]
            form.instance.usuario_creado = request.user
            form.instance.actualizar_saldo()
            form.instance.tipo = tipo
            form.instance.es_vale =True
            
            for index,x in enumerate(cascos):
                if request.POST.getlist("casco")[index] :
                    cantidad_casco = int(request.POST.getlist("casco")[index])
                    if  cantidad_casco > 0:
                        total += cantidad_casco*x.valor
                        if TotalesMovimientosMeses.objects.filter(date_mes=fecha.strftime("%b-%Y").upper()).exists():
                            total_obj=TotalesMovimientosMeses.objects.get(date_mes=fecha.strftime("%b-%Y").upper())
                            total_obj.cantidad_entrada += float(total)
                            

                        else:
                            total_obj=TotalesMovimientosMeses.objects.create(cantidad_entrada=total, date_mes=fecha.strftime("%b-%Y").upper())
                        
                        if tipo == 1:
                            resultado = EnviarEntradaUsado(x.codigo, float(cantidad_casco*x.valor), folio, float(float(cantidad_casco*x.valor)))

                            if resultado != 200:
                                messages.error(
                                    request, resultado
                                )
                                return redirect("app:kardex_entradas_almacen")
                        form.instance.save()
                        total_obj.save()
                        MovimientoCascoDetalles.objects.create(
                            casco=x, movimiento=form.instance, cantidad_origen_local=cantidad_casco, cantidad_origen_pendiente=cantidad_casco)

            form.instance.cantidad_origen = total
            form.instance.cantidad_restante = total
            form.instance.fecha_year_mes = total_obj
            form.instance.save()
            
            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("app:kardex_entradas_almacen")

    return render(
        request, 
        "app/movimiento.html", 
        {
            "title": "Agregar movimiento",
            "form": form,
            "cascos": cascos,
        }
    )

def ver_vale(request, id):
    movimiento = get_object_or_404(
        MovimientoCasco,
        id=id,
    )
    MovimientoFS = modelformset_factory(
        MovimientoCascoDetalles,
        form=MovimientoCascoDetallesFS,
        extra=0,
    )
    formset = MovimientoFS(
        queryset=movimiento.detalles.all() 
    )

    return render_pdf(
        debug=request.GET.get("debug"),
        template="app/ver_vale.html",
        name="movimiento-{}".format(movimiento.folio),
        context={
            "movimiento": movimiento,
            "title": "VALE {}".format(movimiento.folio),
            "request": request,
            "formset": formset,
        },
    )

@login_required
def catalogo_cascos(request):
    """
    Pantalla donde muestra los catalagos de casos
    """
    
    objects = Casco.objects.all()
    return render(
        request, 
        "app/catalogo_cascos.html", 
        {
            "title": "Catálogo de cascos",
            "objects": objects,
        }
    )

@login_required
def editar_casco(request, id):
    """
    Pantalla para editar un casco del catalogo
    """
    casco = get_object_or_404(Casco, id=id)
    form = CascoForm(instance=casco)

    if request.method == "POST":

        form = CascoForm(
            request.POST, instance=casco, usuario=request.user
        )

        if form.is_valid():
            form.save()
            messages.success(
                request, "Los datos han sido editados correctamente."
            )
            return redirect("app:catalogo_cascos")

    return render(
        request,
        "app/casco.html",
        {
            "title": "Editar Casco",
            "form": form,
        }
    )

@login_required
def agregar_casco(request):
    """
    Pantalla para agregar un casco 
    """
    instance = Casco()
    form = CascoForm(instance=instance)

    if request.method == "POST":

        form = CascoForm(
            request.POST, instance=instance, usuario=request.user
        )

        if form.is_valid():
            form.save()
            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("app:catalogo_cascos")

    return render(
        request, 
        "app/casco.html", 
        {
            "title": "Agregar casco",
            "form": form,
        }
    )
  
@login_required
def catalogo_clientes(request):
    """
    Pantalla inicial
    """

    filtro_form = FiltroClientesForm(request.GET)

    objects = Cliente.search_objects(request)

    if request.GET.get("exportar_excel"):
        return exportar_excel.catalogo_clientes(objects)

    return list_view(
        request,
        objects,
        template="app/catalogo_clientes.html",
        context={
            "title": "Catálogo de clientes",
            "NODO_BREADCRUMB": "app:catalogo_clientes",
            "exportar_excel": True,
            "filtro_form":filtro_form,
        },
    )
    
@login_required
def agregar_cliente(request):
    """
    Pantalla para agregar un cliente 
    """
    instance = Cliente()
    form = ClienteForm(instance=instance)

    if request.method == "POST":

        form = ClienteForm(
            request.POST, instance=instance, usuario=request.user
        )

        if form.is_valid():
            form.save()
            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("app:catalogo_clientes")

    return render(
        request, 
        "app/cliente.html", 
        {
            "title": "Agregar cliente",
            "form": form,
        }
    )

@login_required
def editar_cliente(request, id):
    """
    Pantalla para editar un cliente del catalogo
    """
    cliente = get_object_or_404(Cliente, id=id)
    form = ClienteForm(instance=cliente)
    cascos = MovimientoCasco.objects.filter(cliente_id=id).order_by("fecha")

    if request.method == "POST":

        form = ClienteForm(
            request.POST, instance=cliente, usuario=request.user
        )

        if form.is_valid():
            form.save()
            messages.success(
                request, "Los datos han sido editados correctamente."
            )
            return redirect("app:catalogo_clientes")

    return render(
        request,
        "app/cliente.html",
        {
            "title": "Editar Cliente",
            "form": form,
            "cascos": cascos,
        }
    )

"""
Ordenes de compra
"""

@login_required
def ordenes_compra_domicilio(request):
    """
    Pantalla para listado de ordes de compra (domicilio)
    """
    filtro_form = FiltroClientesForm(request.GET)


    return render(
        request,
        "app/ordenes_compra_domicilio.html",
        {
            "title": "Ordenes de compra a domilicio",
            "NODO_BREADCRUMB": "app:ordenes_compra_domicilio",
            "filtro_form": filtro_form,
        }
    )

@login_required 
def notas_credito(request):
    rget = request.GET.copy()
    form = FiltroForm(request.GET.copy(), usuario=request.user)
    objects = NotaCredito.objects.all()

    if form.is_valid():
        rget["desde"] = form.cleaned_data["desde"]
        rget["hasta"] = form.cleaned_data["hasta"]
        rget["q"] = form.cleaned_data["q"]
        objects = NotaCredito.get_objects(rget)

    return list_view(
        request,
        objects,
        template="app/notas_credito.html",
        context={
            "filtro_form":form
            
        },

    ) 
@login_required
def nota_credito(request, id):
    nota = NotaCredito.objects.get(id=id)
    cascos = Casco.objects.all()

    return render(
        request,
        "app/nota_credito.html",
        {
            "title": "Nota de credito",
            "nota": nota,
        }
    )
@login_required
def agregar_nota_credito(request,id):
    movimiento=get_object_or_404(MovimientoCasco,id= id, tipo=1)
    facturas = MovimientoCasco.objects.filter(cliente= movimiento.cliente, es_garantia=False, es_vale=False, pagado=False).order_by( "fecha", "folio")
    nota = NotaCredito.objects.all().first()
    if nota:
        folio = f"NCC-{nota.id}"
    else:
        folio = "NCC-0"

    cascos = Casco.objects.all()
    for f in facturas:
        hg = []
        for index,c in enumerate(cascos):
            suma_q = f.detalles.filter(casco=c).annotate(orig_base1=ExpressionWrapper(F("cantidad_origen_local") * F("casco__valor"), output_field=FloatField())).aggregate(orig_base1__sum=Sum("orig_base1"), cantidad_origen_local__sum=Sum("cantidad_origen_local"))
            suma = to_int(suma_q["cantidad_origen_local__sum"])
            total_folio = to_int(suma_q["orig_base1__sum"])
            hg.insert(index,suma)
        f.total_folio = total_folio
        f.sums_cascos = hg
    return list_view(
        request,
        facturas,
        template="app/agregar_nota_credito.html",
        context={
            "movimiento":movimiento,
            "cascos":cascos,
            "folio":folio
        },
    ) 

def gmroi_cliente(request, id):
    form=FiltroForm(request.GET.copy())
    cliente = Cliente.objects.get(id=id)
    entradas = MovimientoCasco.objects.filter( cliente=id).order_by("fecha_year_mes__date_mes","fecha_year_mes__id").distinct("fecha_year_mes__date_mes")
    return render(
        request,
        "app/gmroi.html",
        {
            "total": entradas,
            "form":form,
        }
    )


@login_required 
def agregar_orden_compra_domicilio(request):
    """
    Pantalla para agregar oredenes de compra a domilicio
    """

    fecha_hoy = timezone.now()
    instance = OrdenCompraDomicilio()
    form = OrdenCompraDomicilioForm(instance=instance)

    return render(
        request,
        "app/orden_compra_domicilio.html",
        {
            "title": "Agregar orden de compra",
            "NODO_BREADCRUMB": "app:ordenes_compra_domicilio",
            "form":form,
            "fecha_hoy":fecha_hoy,
        }
    )

@login_required
def ajax_get_cliente(request, term):

    objects = Cliente.objects.all()

    try:
        cliente = objects.get(nombre=term)
    except Cliente.DoesNotExist:
        return json_response({
                "error":f"No se encontró el cliente: {term}"
            })

    return json_response({
        "id": cliente.id,
        "label": cliente.get_input_display(),
        "nombre": cliente.nombre,
    })

@login_required
def ajax_clientes_ac(request):
    limit = request.GET.get("limit", 10)
    rget = request.GET.copy()
    rget["q"] = request.GET.get("term", "").strip()

    objects = Cliente.get_objects(rget)
    objects = objects.filter(movimiento__isnull=False)

    objects = objects.annotate(saldo_total=Sum('movimiento__cantidad_restante', filter=Q(movimiento__es_vale=True)))
    
    clientes_dict = []

    for o in objects[:10]:
        clientes_dict.append(
            {
                "id":o.id,
                "label": o.get_input_display(),
                "nombre": o.nombre,
                "telefono": o.telefono,
                "saldo": o.saldo_total
            }
        )
    return json_response(clientes_dict)

@login_required
def ajax_crear_nota_credito(request):
    data = {}
    data_nc_detalles =[]
    data_nc = {}
    try:
        vale_id = request.POST["vale"]
        facturas = eval(request.POST["facturas"])
        movimiento = MovimientoCasco.objects.get(id=vale_id)
        nota = NotaCredito.objects.create(vale=movimiento, folio=request.POST["folio"])
        restante=  movimiento.cantidad_restante if movimiento.cantidad_restante else 0
        for f in facturas:
            valor = float(f["valor"])
            fact_obj = MovimientoCasco.objects.get(id=f["id"])
            fact_obj.cantidad_restante -= valor
            if(fact_obj.cantidad_restante == 0.00 ):
                fact_obj.pagado = True
            nota.factura.add(fact_obj)
            restante -=valor
            fact_obj.save()

            if valor > 0:
                detalle_nota_factura = PagoFactura.objects.create(nota=nota, factura=fact_obj, cantidad=f["valor"]).save()
                data_nc_detalles.append(
                    {
                        "producto_codigo": fact_obj.casco.codigo,
                        "cantidad": float(f["valor"]),
                        "precio_unitario": float(fact_obj.casco.valor),
                        "factura": fact_obj.id_admintotal,
                        "um": { "nombre": "PIEZA", "factor":1},
                    }
                )

        movimiento.cantidad_restante = round(restante)
        movimiento.save()
        data_nc = {
            "cliente": movimiento.cliente.id_admintotal,
            "fecha": datetime.datetime.now().strftime("%Y-%m-%d"),
            "comentarios": "Nota de credito desde proyecto cascos usados",
            "detalles": data_nc_detalles,
        }
        resultado = EnviarNotaCredito(data_nc)
        messages.success(
            request, "Nota de crédito generada con éxito"
        )
        
    except:
        error = ["Error al crear nota de credito"]
        data["errors"] = error
    return json_response(data)


@login_required
def ajax_get_folios_by_proveedor(request):

    proveedor = int(request.GET.get("proveedor", "").strip())
    term = request.GET.get("term", "").strip()
    tipo = int(request.GET.get("tipo"))

    proveedor = Proveedor.objects.get(id=proveedor)
    

    cantidad_casco ={}
    cascos = Casco.objects.all()
    for a in cascos:
        cantidad_casco[a.codigo]=0
    
    if tipo == 3: 
        movimiento = MovimientoCasco.objects.filter(folio__icontains=term, tipo=1, salida_proveedor__isnull=True)

    else:
        detalles = MovimientoCascoDetalles.objects.filter(proveedor=proveedor)
        movimiento = MovimientoCasco.objects.filter(detalles__in=detalles, folio__icontains=term, es_garantia=True, salida_proveedor__isnull=True)
    
    data=[]
    for m in movimiento:
        cant_casco_tmp = dict(cantidad_casco)
        for d in m.detalles.all():
            cant_casco_tmp[d.casco.codigo] += d.cantidad_origen_local
        
        data_tmp ={
            "label":m.folio,
            "cascos": cant_casco_tmp,
        }
        data.append(data_tmp)
    return json_response(data)

@login_required
def kardex_almacen(request):
    form = FiltroForm(request.GET.copy())
    cascos =Casco.objects.all() 

    if form.is_valid():
        desde = form.cleaned_data["desde"]
        haste = form.cleaned_data["hasta"]
        
        inv_total = sum(casco.get_inventario for casco in cascos)
        clientes = Cliente.objects.filter(movimiento__isnull=False).order_by("id").distinct("id")
        for c in clientes:
            sum_cascos_entrada = []
            sum_cascos_salida = []

            for index, casco in enumerate(cascos):
                detalles = MovimientoCascoDetalles.objects.filter(movimiento__cliente=c, casco=casco).aggregate(
                    total_salidas=Sum("cantidad_origen_local", filter=Q(movimiento__tipo=2)),
                    total_entradas=Sum("cantidad_origen_local", filter=Q(movimiento__tipo=1))
                    )
                sum_cascos_entrada.insert(index, detalles.get("total_entradas"))
                sum_cascos_salida.insert(index, detalles.get("total_salidas"))
                
            c.total_cascos= zip(sum_cascos_entrada, sum_cascos_salida)

    return list_view(
        request,
        clientes,
        template = "app/kardex_almacen.html",
        context = {
            "form":form,
            "cascos":cascos,
            "inv_total":inv_total,
        },
    )

@login_required
def configuracion_inventario(request):
    cascos = Casco.objects.all()

    if request.method=="POST":
        for c in cascos:
            cantidad = request.POST.get(f"inventario-{c.id}")
            inventario=InventarioAlmacen.objects.filter(casco=c).first()
            if inventario:
                inventario.cantidad=cantidad
            else:
               inventario =InventarioAlmacen(
                   casco=c,
                    cantidad=cantidad    
                    )
            inventario.save() 
    return render(
        request,
        "app/configuracion_inventario.html",
        {
            "cascos":cascos,
        }
        
    )
@login_required
def cancelar_movimiento(request,id):
    mov = get_object_or_404(MovimientoCasco, id=id)
    mov.cancelado=True 
    mov.save()
    return redirect("app:kardex_entradas_almacen")
