"""homedepot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include
from . import views

#app_name = "homedepot"


def trigger_error(request):
    division_by_zero = 1 / 0


urlpatterns = [
    
    # ...
]

app_patterns = ([
    path("", views.index, name="index"),
    path("kardex_entradas_almacen/", views.kardex_entradas_almacen, name="kardex_entradas_almacen"),
    path("kardex_almacen/", views.kardex_almacen, name="kardex_almacen"),
    path("contador_usados/", views.contador_usados, name="contador_usados"),
    path("kardex/", views.kardex_usados, name="kardex"),
    path("salidas_proveedor/", views.salidas_proveedor, name="salidas_proveedor"),
    path("kardex_garantia/", views.kardex_garantia, name="kardex_garantia"),
    path("contador_cliente/<int:id>/", views.contador_cliente, name="contador_cliente"),
    path("sincronizar_kardex/", views.sincronizar_kardex, name="sincronizar_kardex"),
    path("regenerar_existencias_kardex/", views.regenerar_existencias_kardex, name="regenerar_existencias_kardex"),
    path("catalogo_cascos/", views.catalogo_cascos, name="catalogo_cascos"),
    path("configuracion_inventario/", views.configuracion_inventario, name="configuracion_inventario"),
    path("configuracion_inventario/", views.configuracion_inventario, name="configuracion_inventario"),
    
    path("catalogo_clientes/", views.catalogo_clientes, name="catalogo_clientes"),
    path("agregar_casco/", views.agregar_casco, name="agregar_casco"),
    path("agregar_cliente/", views.agregar_cliente, name="agregar_cliente"),
    path("editar_casco/<int:id>", views.editar_casco, name="editar_casco"),
    path("editar_cliente/<int:id>", views.editar_cliente, name="editar_cliente"),
    path("agregar_entrada/", views.agregar_entrada, name="agregar_entrada"),
    path("agregar_salida_garantia_proveedor/",views.agregar_salida_garantia_proveedor, name="agregar_salida_garantia_proveedor"),
    path("agregar_salida_usado_proveedor/",views.agregar_salida_usado_proveedor, name="agregar_salida_usado_proveedor"),
    path("agregar_salida/", views.agregar_salida, name="agregar_salida"),
    path("graficas_movimientos/", views.graficas_movimientos, name="graficas_movimientos"),
    path("cancelar_movimiento/<int:id>", views.cancelar_movimiento, name="cancelar_movimiento"),
    
    path("ver_vale/<int:id>/", views.ver_vale, name="ver_vale"),
    #paht("nota_de_credito/<int:id>", views.ver_nota_de_credito, name="ver_nota_de_credito"),
    path("notas_credito/", views.notas_credito, name="notas_credito"),
    path("nota_credito/<int:id>", views.nota_credito, name="nota_credito"),
    path("agregar_nota_credito/<int:id>", views.agregar_nota_credito, name="agregar_nota_credito"),
    path("gmroi_cliente/<int:id>", views.gmroi_cliente, name="gmroi_cliente"),

    path("ordenes_compra_domicilio/", views.ordenes_compra_domicilio, name="ordenes_compra_domicilio"),
    path("agregar_orden_compra_domicilio/", views.agregar_orden_compra_domicilio, name="agregar_orden_compra_domicilio"),
    path("ajax_get_cliente/<str:term>/", views.ajax_get_cliente, name="ajax_get_cliente"),
    path("ajax_clientes_ac/", views.ajax_clientes_ac, name="ajax_clientes_ac"),
    path("ajax_crear_nota_credito/", views.ajax_crear_nota_credito, name="ajax_crear_nota_credito"),
    path("ajax_get_folios_by_proveedor/", views.ajax_get_folios_by_proveedor,name="ajax_get_folios_by_proveedor"),
    path('sentry-debug/', trigger_error),

    
], "app")

urlpatterns = [
    path("", include(app_patterns)),

    path("configuracion/", include("configuracion.urls")),
    path("usuarios/", include("usuarios.urls")),
    path("atshell/", include("atshell.urls")),
]

# archivos subidos al sistema
urlpatterns = urlpatterns + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

# static files
urlpatterns = urlpatterns + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
)
