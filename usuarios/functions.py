from .models import Perfil


def get_perfil(usuario):
    """
    Retorna el perfil relacionado al usuario especificado, si no existe lo crea.
    """
    if not hasattr(usuario, "perfil"):
        from usuarios.models import Perfil

        Perfil.objects.create(usuario=usuario).save()
    return usuario.perfil

def es_usuario_o_suplencia(usuario_a_validar, usuario_que_valida):

	if usuario_a_validar == usuario_que_valida:
		return True

	return (
		usuario_a_validar.perfil.suplencia and 
		usuario_a_validar.perfil.suplencia == usuario_que_valida
	)