from django import forms
from tools.forms import BootstrapModelForm, BootstrapForm
from .models import User, Perfil
from .functions import get_perfil
from tools.fields import CustomFileField
from tools.forms import CalendarDateField

class UsuarioModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        label = obj.get_full_name()
        if not label:
            label = obj.username
        return label

class UsuarioPerfilForm(BootstrapModelForm):
    username = forms.CharField(label="Nombre de usuarios")
    password = forms.CharField(
        widget=forms.PasswordInput, 
        required=False, 
        label="Contraseña",
        strip=False
    )
    re_password = forms.CharField(
        widget=forms.PasswordInput,
        required=False,
        label="Confirmar contraseña",
        strip=False
    )

    telefono = forms.CharField(required=False, label="Teléfono")
    telefono_extension = forms.CharField(required=False, label="Ext")
    firma = CustomFileField(required=False, image=True)
    tema_tamano_fuente = forms.IntegerField(
        required=False, initial=14, label="Tamaño de fuente (px)"
    )


    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "email",
            "is_active",
        )

    def __init__(self, *args, **kwargs):
        super(UsuarioPerfilForm, self).__init__(*args, **kwargs)
        instance = getattr(self, "instance", None)
        if instance and instance.id:
            perfil = get_perfil(instance)
            self.fields["username"].widget.attrs["readonly"] = True
            self.fields["password"].widget.attrs["readonly"] = True
            self.fields["username"].initial = instance.username

            self.fields["telefono"].initial = perfil.telefono
            self.fields[
                "telefono_extension"
            ].initial = perfil.telefono_extension
            self.fields["firma"].initial = perfil.firma
            self.fields[
                "tema_tamano_fuente"
            ].initial = perfil.tema_tamano_fuente
        else:
            self.fields["password"].required = True
            self.fields["re_password"].required = True

    def clean_tema_tamano_fuente(self):
        size = self.cleaned_data["tema_tamano_fuente"]
        min_size = 12
        max_size = 21
        if size and size < min_size:
            raise forms.ValidationError(
                "El valor mínimo es de {}".format(min_size)
            )

        if size and size > max_size:
            raise forms.ValidationError(
                "El valor máximo es de {}".format(max_size)
            )

        return size

    def clean_email(self):
        """
        Validación del email de usuario
        """
        email = self.cleaned_data.get("email")
        if not email:
            return email

        instance_id = None
        instance = getattr(self, "instance", None)
        if instance and instance.id:
            instance_id = instance.id

        exists = User.objects.filter(email=email).exclude(id=instance_id)
        if exists:
            raise forms.ValidationError(
                "Ya existe un usuario con el email {}".format(email)
            )

        return email

    def clean_username(self):
        """
        Validación del nombre de usuario
        """
        username = self.cleaned_data.get("username", "").strip().lower()
        instance_id = None
        instance = getattr(self, "instance", None)
        if instance and instance.id:
            instance_id = instance.id
        else:
            if not username:
                raise forms.ValidationError("Este campo es requerido")

        exists = User.objects.filter(username=username).exclude(id=instance_id)
        if exists:
            raise forms.ValidationError(
                "Ya existe un nombre de usuario {}".format(username)
            )

        return username

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if password.startswith(" ") or password.endswith(" "):
            raise forms.ValidationError(
                "La contraseña no puede iniciar o finalizar con espacio"
            )

        return password

    def clean(self):
        """
        Validación del formulario en general
        """
        data = self.cleaned_data
        instance = getattr(self, "instance", None)
        if not instance or not instance.id:
            if data.get("password") != data.get("re_password"):
                raise forms.ValidationError(
                    {
                        "password": "Las contraseñas no coinciden",
                        "re_password": "Las contraseñas no coinciden",
                    }
                )

            self.instance.username = data.get("username")
            self.instance.set_password(self.cleaned_data.get("password"))

        suplencia = self.cleaned_data.get("suplencia")
        suplencia_desde = self.cleaned_data.get("suplencia_desde")
        suplencia_hasta = self.cleaned_data.get("suplencia_hasta")

        if suplencia and not (suplencia_desde and suplencia_hasta):
            raise forms.ValidationError({
                "suplencia_desde": "Este campo es obligatorio",
                "suplencia_hasta": "Este campo es obligatorio",
            })


        if suplencia_desde and suplencia_hasta:
            if suplencia_desde > suplencia_hasta:
                raise forms.ValidationError({
                    "suplencia_desde": (
                        "El inicio de suplencia no puede ser mayor a {}".format(
                            suplencia_hasta.strftime("%d/%m/%Y")
                        )
                    )
                })

        return data

    def save_perfil(self, request=None):
        """
        Guarda el perfil de la instancia User
        """
        if not self.instance.id:
            raise Exception("El usuario no ha sido guardado aún.")

        perfil = get_perfil(self.instance)
        perfil.telefono = self.cleaned_data.get("telefono")
        perfil.telefono_extension = self.cleaned_data.get("telefono_extension")
        perfil.firma = self.cleaned_data.get("firma")
        perfil.zona_horaria = self.cleaned_data.get("zona_horaria")
        perfil.tema_tamano_fuente = self.cleaned_data.get("tema_tamano_fuente")
        perfil.centro_responsabilidad = self.cleaned_data.get("centro_responsabilidad")
        perfil.suplencia = self.cleaned_data.get("suplencia")
        perfil.suplencia_desde = None
        perfil.suplencia_hasta = None

        if perfil.suplencia:
            perfil.suplencia_desde = self.cleaned_data.get("suplencia_desde")
            perfil.suplencia_hasta = self.cleaned_data.get("suplencia_hasta")

        if request:
            if not perfil.usuario_creado:
                perfil.usuario_creado = request.user

            perfil.usuario_modificado = request.user

        perfil.save()
        return perfil

class ImportarUsuariosForm(BootstrapForm):
    archivo = CustomFileField(required=True, excel=True)
    cambiar_password = forms.BooleanField(
        initial=False, 
        required=False, 
        label="Cambiar contraseña a usuarios existentes",
    )

class SolicitudCambioPasswordForm(BootstrapForm):
    username = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    
    def clean(self):
        data = self.cleaned_data
        username = data.get("username", "").strip().lower()
        email = data.get("email", "").strip()

        if not email and not username:
            raise forms.ValidationError({
                "username": (
                    "Debe especificar el nombre de usuario o email "
                    "para continuar"
                )
            })

        return data

class ResetPasswordForm(BootstrapForm):
    new_password = forms.CharField(
        widget=forms.PasswordInput, 
        label="Nueva contraseña",
        strip=False
    )
    confirm_new_password = forms.CharField(
        widget=forms.PasswordInput, 
        label="Confirmar nueva contraseña", 
        strip=False
    )

    def clean_new_password(self):
        password = self.cleaned_data.get("new_password")
        if password.startswith(" ") or password.endswith(" "):
            raise forms.ValidationError(
                "La contraseña no puede iniciar o finalizar con espacio"
            )

        return password

    def clean(self):
        data = self.cleaned_data
        if data.get("new_password") != data.get("confirm_new_password"):
            raise forms.ValidationError({
                "new_password": "Las contraseñas no coinciden"   
            })
            
        return data
            
