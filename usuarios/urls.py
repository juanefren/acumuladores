from usuarios import views
from django.urls import path

app_name = "usuarios"

urlpatterns = [
    path("login/", views.login, name="login"),
    path("logout/", views.logout, name="logout"),
    path("acceso_denegado/", views.acceso_denegado, name="acceso_denegado"),
    path("", views.usuarios, name="usuarios"),
    path("roles", views.roles, name="roles"),
    path("perfil", views.perfil, name="perfil"),
    path("editar_usuario/<int:id>/", views.usuario, name="usuario"),
    path("agregar_usuario/", views.usuario, name="usuario"),
    path("inicios_sesion/", views.inicios_sesion, name="inicios_sesion"),
    path("importar_usuarios/", views.importar_usuarios, name="importar_usuarios"),
    path("reset_password/", views.reset_password, name="reset_password"),
    path("reset_password/<str:token>/", views.reset_password, name="reset_password"),
    path(
        "reenviar_correo_bienvenida/<int:id>/",
        views.reenviar_correo_bienvenida,
        name="reenviar_correo_bienvenida",
    ),
    path(
        "ajax/cambiar_password/",
        views.cambiar_password,
        name="cambiar_password",
    ),
    path(
        "ajax/validar_autorizacion/<int:id>/",
        views.ajax_validar_autorizacion,
        name="ajax_validar_autorizacion",
    ),
    path("agregar_rol/", views.agregar_rol, name="agregar_rol"),
    path("editar_rol/<int:id>/", views.editar_rol, name="editar_rol"),
    path("eliminar_rol/<int:id>/", views.eliminar_rol, name="eliminar_rol"),
    # ajax
    path("ajax/set_sidebar/", views.ajax_set_sidebar, name="set_sidebar"),
    path(
        "ajax/set_permiso_rol/<int:id>/",
        views.ajax_set_permiso_rol,
        name="set_permiso_rol",
    ),
]
