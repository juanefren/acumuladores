from django.db import models
from django.contrib.auth.models import User
from tools.functions import send_email, localtime
from django.utils.functional import cached_property
from django.contrib.postgres.fields import ArrayField
from tools.functions import date_to_str, set_crontab
from django.urls import reverse
import pytz
import uuid
import datetime

class Rol(models.Model):
    nombre = models.CharField(max_length=255, unique=True)
    permisos = ArrayField(models.CharField(max_length=255), null=True)

    class Meta:
        ordering = ("nombre",)

    def __str__(self):
        return self.nombre


class Perfil(models.Model):
    """Modelo de perfil de usuario."""

    MX_TIMEZONES = [(x, x) for x in pytz.country_timezones["MX"]]

    usuario_creado = models.ForeignKey(User, null=True, related_name="usuarios_creados", on_delete=models.CASCADE)
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=100, null=True, blank=True)
    telefono_extension = models.CharField(max_length=20, null=True, blank=True)
    firma = models.ImageField(upload_to="firmas", null=True, blank=True)
    sidebar_colapsado = models.BooleanField(default=False)
    zona_horaria = models.CharField(
        max_length=255,
        choices=MX_TIMEZONES,
        null=True,
        blank=True,
        default="America/Mexico_City",
        verbose_name="Zona Horaria",
    )
    tema_tamano_fuente = models.CharField(
        max_length=50, blank=True, null=True, default="14"
    )
    token_reset_password = models.CharField(
        max_length=255, unique=True, null=True
    )
    roles = models.ManyToManyField(Rol, related_name="usuarios")


    def __str__(self):
        return "Perfil de usuario {}".format(self.usuario)

    def get_absolute_url(self):
        return reverse("usuarios:usuario", args=[
            self.usuario_id
        ])

    @property
    def nombre(self):
        return self.usuario.first_name

    @property
    def apellidos(self):
        return self.usuario.last_name
    @property
    def telefonos(self):
        return self.usuario.telefono

    @classmethod
    def get_user_objects(cls, vars):
        objects = User.objects.all()
        q = vars.get("q", "")
        cedis = vars.get("cedis")
        centro_responsabilidad = vars.get("choices")
        status = vars.get("status")

        objects = objects.filter(
            models.Q(username__icontains=q) |
            models.Q(first_name__icontains=q) | 
            models.Q(last_name__icontains=q)
        )

        if status == "activos" or not status:
            objects = objects.filter(is_active=True)
        elif status == "inactivos":
            objects = objects.filter(is_active=False)

        if cedis:
            objects = objects.filter(perfil__cedis=cedis)

        if centro_responsabilidad:
            objects = objects.filter(
                perfil__centro_responsabilidad_id=centro_responsabilidad
            )

        objects = objects.order_by('username')
        return objects

    @cached_property
    def get_roles(self):
        """
        Obtiene roles del perfil más los roles  de las suplencias
        """
        roles = self.roles.all()
        for p in Perfil.objects.filter(suplencia=self.usuario):
            roles = roles | p.roles.all()
        return roles
    
    @cached_property
    def permisos_roles(self):
        """
        Retorna una lista con los permisos de los roles que se
        tienen asignados para este perfil.
        """
        permisos = []
        for p in self.roles.values_list("permisos", flat=True):
            permisos += p
        return permisos

    @cached_property
    def permisos_suplencias(self):
        """
        Retorna una lista con los permisos de las suplencias que se
        tienen asignadas para este perfil.
        """
        suplencias = []
        hoy = localtime().date()
        objects = Perfil.objects.filter(
            suplencia=self.usuario,
            suplencia_desde__lte=hoy,
            suplencia_hasta__gte=hoy
        )
        for p in objects.values_list("roles__permisos", flat=True):
            suplencias += p

        return suplencias

    def tiene_permiso(self, permiso):
        """Validación de permiso de un usuario."""
        if self.usuario.is_superuser:
            return True

        permisos = self.permisos_roles
        suplencias = self.permisos_suplencias
        return permiso in set(permisos + suplencias)

    def notificar_cambio_password(self):
        """Enviamos correo de bienvenida al usuario"""
        email = self.usuario.email

        if not email:
            return False

        return send_email(
            to=[email],
            subject="Cambio de contraseña",
            template={
                "name": "usuarios/emails/cambio_password.html",
                "context": {"perfil": self},
            },
        )

    def notificar_inicio_suplencia(self):
        """
        Docs
        """
        from configuracion.models import Configuracion
        if not self.suplencia or not self.suplencia.email:
            return False

        email = self.suplencia.email
        c = Configuracion.objects.get()
        html_mensaje = c.get_html_mensaje(
            field="html_mensaje_inicio_suplencia",
            placeholders={
                "_usuario_suplantante_": self.suplencia.get_full_name(),
                "_usuario_suplantado_": self.usuario.get_full_name(),
                "_fecha_inicio_suplencia_": self.suplencia_desde.strftime("%d/%m/%Y"),
                "_fecha_fin_suplencia_": self.suplencia_hasta.strftime("%d/%m/%Y"),
            }
        )
        set_crontab(
            "notificar_fin_suplencia",
            setall="{} *".format(self.suplencia_hasta.strftime("30 11 %d %m")),
            command_opts={"--id": self.id},
            auto_delete=True,
        )
        return send_email(
            to=[email],
            subject="Notificación nueva suplencia",
            template={
                "name": "usuarios/emails/suplencia.html",
                "context": { "perfil": self, "html_mensaje": html_mensaje},
            },
        )

    def notificar_fin_suplencia(self):
        """
        Docs
        """
        from configuracion.models import Configuracion
        if not self.suplencia or not self.suplencia.email:
            return False

        email = self.suplencia.email
        c = Configuracion.objects.get()
        html_mensaje = c.get_html_mensaje(
            field="html_mensaje_fin_suplencia",
            placeholders={
                "_usuario_suplantante_": self.suplencia.get_full_name(),
                "_usuario_suplantado_": self.usuario.get_full_name(),
                "_fecha_inicio_suplencia_": self.suplencia_desde.strftime("%d/%m/%Y"),
                "_fecha_fin_suplencia_": self.suplencia_hasta.strftime("%d/%m/%Y"),
            }
        )
        return send_email(
            to=[email],
            subject="Finalización de suplencia",
            template={
                "name": "usuarios/emails/suplencia.html",
                "context": { "perfil": self, "html_mensaje": html_mensaje},
            },
        )

    def enviar_email_bienvenida(self):
        """Enviamos correo de bienvenida al usuario"""
        email = self.usuario.email

        if not email:
            return False

        return send_email(
            to=[email],
            subject="Correo de bienvenida",
            template={
                "name": "usuarios/emails/correo_bienvenida.html",
                "context": {"perfil": self},
            },
        )

    def set_token_reset_password(self):
        """Asigna el token para el cambio de contraseña."""
        token = str(uuid.uuid1())
        while Perfil.objects.filter(token_reset_password=token).exists():
            token = str(uuid.uuid1())

        self.token_reset_password = token
        
    def enviar_email_reset_password(self):
        """Envía correo con liga para asignar nueva contraseña"""
        from configuracion.models import Configuracion
        email = self.usuario.email

        if not email:
            return False

        configuracion = Configuracion.objects.get()
        
        self.set_token_reset_password()
        self.save()

        return send_email(
            to=[email],
            subject="Solicitud de cambio de contraseña",
            template={
                "name": "usuarios/emails/reset_password.html",
                "context": {
                    "perfil": self,
                    "link_reset_password": "{}{}".format(
                        configuracion.get_url_proyecto(),
                        reverse(
                            "usuarios:reset_password", 
                            args=[self.token_reset_password]
                        )
                    )
                },
            },
        )

class InicioSesion(models.Model):
    ip = models.CharField(max_length=50)
    usuario = models.ForeignKey(
        User, 
        related_name="inicio_sesiones", 
        on_delete=models.PROTECT,
        null=True
    )
    nombre_usuario = models.CharField(max_length=255, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    user_agent = models.CharField(max_length=255)
    exitoso = models.BooleanField(default=False)
    motivo = models.TextField()

    class Meta:
        ordering = ("-fecha",)

    def __str__(self):
        return "[{}] {} - {}".format(
            self.ip, 
            date_to_str(self.fecha), 
            self.user
        )

    @classmethod
    def get_objects(cls, vars):
        objects = cls.objects.all()
        q = vars.get("q", "")
        if q:
            objects = objects.filter(
                models.Q(user_agent__icontains=q)
                | models.Q(nombre_usuario__icontains=q)
                | models.Q(ip__icontains=q)
            )
        return objects