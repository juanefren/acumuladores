from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from .forms import (
    UsuarioPerfilForm, 
    ImportarUsuariosForm, 
    SolicitudCambioPasswordForm, 
    ResetPasswordForm,
)
from .functions import get_perfil
from django.urls import reverse
from tools.functions import (
    json_response,
    list_view,
    get_user_ip,
    get_hoja_excel,
    xls_text_value,
    error_response,
)
from django.views.decorators.http import require_http_methods
from django.contrib.auth import (
    authenticate,
    login as login_user,
    logout as logout_user,
)
from tools.classes import ExcelFile
import time
from tools.forms import EMPTY_CHOICE
from django.views.decorators.http import require_http_methods
from .models import InicioSesion
from usuarios.models import Perfil


def login(request):
    from django.urls import NoReverseMatch
    """
    Render de login de usuarios
    """
    next = request.GET.get('next','/')
    if request.method == "POST":
        username = request.POST.get("username", "").lower()
        password = request.POST.get("password", "")
        registro_inicio_sesion = InicioSesion(
            ip=get_user_ip(request),
            user_agent=request.META.get("HTTP_USER_AGENT", ""),
            nombre_usuario=username,
            exitoso=False,
        )
        usuario = User.objects.filter(username=username).first()
        user = authenticate(username=username, password=password)
        if not user:
            registro_inicio_sesion.save()
            messages.error(
                request, "Nombre de usuario o contraseña incorrectos"
            )
            return redirect(".")

        login_user(request, user)

        registro_inicio_sesion.usuario = usuario
        registro_inicio_sesion.exitoso = True
        registro_inicio_sesion.save()

        messages.success(request, "Bienvenido {}".format(user.username))
        try:
            return redirect(next)
        except NoReverseMatch:
            return redirect("/")
    solicitar_cambio_pass_form = SolicitudCambioPasswordForm()

    return render(
        request, 
        "usuarios/login.html", 
        {
            "title": "Iniciar Sesión", 
            "solicitar_cambio_pass_form": solicitar_cambio_pass_form
        }
    )


def logout(request):
    """
    Logout de usuarios y redirect a login.
    """
    logout_user(request)
    return redirect("/")

def reset_password(request, token=None):
    """
    Envía la solicitud de cambio de contraseña al usuario, cuando token es
    especificado renderea el formulario para asignar nueva contraseña y 
    recibe el post para asignar la nueva contraseña.
    """
    if token:
        perfil = get_object_or_404(Perfil, token_reset_password=token)
        form = ResetPasswordForm()
        if request.method == "POST":
            form = ResetPasswordForm(request.POST)
            if form.is_valid():
                new_password = form.cleaned_data.get("new_password")
                perfil.usuario.set_password(new_password)
                perfil.usuario.save()

                perfil.token_reset_password = None
                perfil.save()
                perfil.notificar_cambio_password()

                messages.success(
                    request, 
                    "Se ha actualizado la contraseña para el usuario {}".format(
                        perfil.usuario.username
                    )
                )
                return redirect(reverse("usuarios:login"))

        return render(
            request, 
            "usuarios/reset_password_form.html", 
            {"form": form, "perfil":perfil}
        )


    solicitar_cambio_pass_form = SolicitudCambioPasswordForm(request.POST)
    back_url = reverse("usuarios:login")
    if solicitar_cambio_pass_form.is_valid():
        username = solicitar_cambio_pass_form.cleaned_data.get("username")
        email = solicitar_cambio_pass_form.cleaned_data.get("email")
        user = None

        if username:
            user = User.objects.filter(username=username).first()
            if not user:
                error_message = "El usuario especificado no existe."
                return redirect(
                    "{}?error_reset_pass={}".format(
                        back_url, error_message)
                )

            if not user.email:
                error_message = "El usuario no cuenta con email especificado."
                return redirect(
                    "{}?error_reset_pass={}".format(
                        back_url, error_message)
                )

        elif email:
            user = User.objects.filter(email=email).first()
            if not user:
                error_message = "El usuario especificado no existe."
                return redirect(
                    "{}?error_reset_pass={}".format(
                        back_url, error_message)
                )

        user.perfil.enviar_email_reset_password()
        return redirect(
            "{}?success_reset_pass={}".format(
                back_url, 
                "Se ha enviado un correo a {} con la liga para "
                "ingresar una nueva contraseña.".format(user.email)
            )
        )

    error_message = ", ".join(solicitar_cambio_pass_form.errors.values())
    return redirect(
        "{}?error_reset_pass={}".format(
            back_url, error_message)
    )


def acceso_denegado(request):
    """
    Render del template de acceso denegado en caso de que el usuario
    ya este autenticado, si no se hace redirect a login.
    """
    if not request.user.is_authenticated:
        return redirect(
            "/usuarios/login/?next={}".format(request.GET.get("next", "/"))
        )

    template_base = "app/clean_base.html"
    if request.user.is_authenticated:
        template_base = "app/base.html"

    context = {
        "title": "Acceso Denegado",
        "template_base": template_base,
        "permiso": request.GET.get("permiso"),
    }
    return render(request, "usuarios/acceso_denegado.html", context)


def roles(request):
    """
    Render de la configuración de roles de usuarios.
    """
    roles = Rol.objects.all()
    data = []

    return render(
        request,
        "usuarios/roles.html",
        {
            "title": "Permisos y roles",
            "roles": roles,
            "PERMISOS_UBICACIONES": PERMISOS_UBICACIONES,
            "NODO_BREADCRUMB": "usuarios:roles",
        },
    )


def usuarios(request):
    """
    Listado de usuarios
    """
    objects = Perfil.get_user_objects(request.GET)
    #filtro_form = FiltroForm(request.GET, usuario=request.user)
    choices_cr = list(EMPTY_CHOICE)

    #for cr in CentroResponsabilidad.objects.all():
    #    choices_cr.append((cr.id, str(cr)))
    
    #filtro_form.fields["choices"].choices = choices_cr

    if request.GET.get("excel"):
        return set_excel_usuarios(objects)

    return list_view(
        request,
        objects,
        template="usuarios/usuarios.html",
        context={
            "title": "Listado de usuarios",
            "NODO_BREADCRUMB": "usuarios:usuarios",
            "exportar_excel": True,
            #"filtro_form": filtro_form,
        },
    )


def set_excel_usuarios(objects):

    hoja = ExcelFile(nombre_hoja="Usuarios")

    hoja.write_title("Usuario")
    hoja.write_title("Nombre")
    hoja.write_title("Apellido")
    hoja.write_title("Correo electrónico")
    hoja.write_title("Teléfono")
    hoja.write_title("Tel Ext.")
    hoja.write_title("Firma")
    hoja.write_title("CR")
    hoja.write_title("Roles")
    hoja.write_title("Zona horaria")
    hoja.write_title("Creado")
    hoja.write_title("Activo")
    hoja.write_title("Último inicio sesión")

    for u in objects:
        cr = None
        if u.perfil.centro_responsabilidad:
            cr = "{} - {}".format(
                u.perfil.centro_responsabilidad.codigo,
                u.perfil.centro_responsabilidad.nombre
            )
        
        hoja.add_row()
        hoja.write_str(u.username)
        hoja.write_str(u.first_name)
        hoja.write_str(u.last_name)
        hoja.write_str(u.email)
        hoja.write_str(u.perfil.telefono)
        hoja.write_str(u.perfil.telefono_extension)
        hoja.write_str(u.perfil.firma)
        hoja.write_str(cr)
        hoja.write_str(
            ", ".join(u.perfil.roles.all().values_list("nombre", flat=True))
        )

        hoja.write_str(u.perfil.zona_horaria)
        hoja.write_str(u.date_joined)
        if u.is_active:
            hoja.write_str("Sí")
        else:
            hoja.write_str("No")

        hoja.write_str(u.last_login)


    return hoja.xlsx_to_response("usuarios.xlsx")


def perfil(request):
    """
    Perfil de usuario.
    """
    form = UsuarioPerfilForm(instance=request.user)
    if request.method == "POST":
        form = UsuarioPerfilForm(
            request.POST, request.FILES, instance=request.user
        )
        form.fields["is_active"].disabled = True
        if form.is_valid():
            form.save()
            form.save_perfil(request=request)

            #del request.session["django_timezone"]

            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("usuarios:perfil")

    return render(
        request,
        "usuarios/perfil.html",
        {
            "title": "Mi Perfil",
            "form": form,
            "NODO_BREADCRUMB": "usuarios:usuarios",
        },
    )


def usuario(request, id=None):
    """
    Agregar o editar un usuario.
    """
    usuario = User()
    title = "Agregar usuario"
    if id:
        usuario = get_object_or_404(User, id=id)
        title = "Editar usuario {}".format(usuario)

    form = UsuarioPerfilForm(instance=usuario)
    if request.method == "POST":
        form = UsuarioPerfilForm(request.POST, request.FILES, instance=usuario)

        if form.is_valid():
            form.save()
            perfil = form.save_perfil(request=request)
            if not id:
                perfil.enviar_email_bienvenida()

            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("usuarios:usuarios")

    return render(
        request,
        "usuarios/usuario.html",
        {"title": title, "form": form, "NODO_BREADCRUMB": "usuarios:usuarios"},
    )


def reenviar_correo_bienvenida(request, id):
    """
    Acción para reenviar el correo de bienvenida al usuario especificado.
    """
    usuario = get_object_or_404(User, id=id)
    if not usuario.email:
        messages.error(
            request,
            "El usuario {} no tiene correo electrónico asignado".format(
                usuario.username
            ),
        )
    else:
        usuario.perfil.enviar_email_bienvenida()
        messages.success(
            request,
            "Se envió el correo de bienvenida a {}".format(usuario.email),
        )
    return redirect(reverse("usuarios:usuario", args=[usuario.id]))


def ajax_set_permiso_rol(request, id):
    """
    Agregar o quitar un permiso al rol especificado.
    """
    rol = get_object_or_404(Rol, id=id)
    permisos = rol.permisos or []
    permiso = request.POST.get("permiso")
    habilitado = int(request.POST.get("habilitado"))

    if permiso is None:
        return json_response(
            {"status": "error", "mensaje": "permiso no fué especificado."}
        )

    if habilitado is None:
        return json_response(
            {"status": "error", "mensaje": "habilitado no fué especificado."}
        )

    if habilitado:
        permisos.append(permiso)
    else:
        if permiso in permisos:
            permisos.remove(permiso)

    rol.permisos = list(set(permisos))
    rol.save()

    return json_response({"status": "success"})


@login_required
def ajax_set_sidebar(request):
    """
    Guarda el estado del sidebar (si se mostrará colapsado o no)
    """
    colapsado = int(request.POST.get("colapsado"))
    request.user.perfil.sidebar_colapsado = colapsado
    request.user.perfil.save()
    return json_response(
        {
            "status": "success",
            "colapsado": request.user.perfil.sidebar_colapsado,
        }
    )


def agregar_rol(request):
    """
    Agregar un nuevo rol.
    """
    back_url = request.POST.get("back_url", "usuarios:roles")
    nombre = request.POST.get("nombre")
    rol = Rol()
    rol.nombre = nombre
    rol.save()
    messages.success(request, "Los datos han sido guardados correctamente.")
    return redirect(back_url)


def editar_rol(request, id):
    """
    Editar el rol especificado
    """
    rol = get_object_or_404(Rol, id=id)
    back_url = request.POST.get("back_url", "usuarios:roles")
    nombre = request.POST.get("nombre")
    rol.nombre = nombre
    rol.save()
    messages.success(request, "Los datos han sido guardados correctamente.")
    return redirect(back_url)


def eliminar_rol(request, id):
    """
    Eliminar el rol especificado.
    """
    rol = get_object_or_404(Rol, id=id)
    back_url = request.POST.get("back_url", "usuarios:roles")
    rol.delete()
    messages.success(request, "Los datos han sido guardados correctamente.")
    return redirect(back_url)


@login_required
@require_http_methods(["POST"])
def cambiar_password(request):
    usuario = request.user
    usuario_id = request.POST.get("usuario_id")
    pass_actual = request.POST.get("pass_actual", "").strip()
    nuevo_pass = request.POST.get("nuevo_pass", "").strip()
    re_nuevo_pass = request.POST.get("re_nuevo_pass", "").strip()
    back_url = request.POST.get("back_url", "/")
    enviar_notificacion_email = False

    if usuario_id:
        usuario = get_object_or_404(User, id=usuario_id)
    else:
        enviar_notificacion_email = True
        if not usuario.check_password(pass_actual):
            messages.error(request, "La contraseña actual es incorrecta.")
            return redirect(back_url)

    if not nuevo_pass:
        messages.error(request, "Se detectó campo de nuevo password vacío..")
        return redirect(back_url)

    if nuevo_pass != re_nuevo_pass:
        messages.error(request, "Las contraseñas no coinciden.")
        return redirect(back_url)

    usuario.set_password(nuevo_pass)
    usuario.save()

    if usuario == request.user:
        login_user(request, usuario)

    perfil = get_perfil(usuario)

    if enviar_notificacion_email:
        perfil.notificar_cambio_password()
    
    messages.success(request, "La contraseña ha sido actualizada.")
    return redirect(back_url)


@login_required
def ajax_validar_autorizacion(request, id):
    usuario = get_object_or_404(User, id=id)
    password = request.POST.get("password")
    autorizar = usuario.check_password(password)

    if not autorizar:
        return json_response(
            {"status": "error", "mensaje": "La contraseña es incorrecta."}
        )

    return json_response({"status": "success"})


def inicios_sesion(request):
    """
    Reporte de inicios de sesión del usuario especificado.
    """
    objects = InicioSesion.get_objects(request.GET)

    return list_view(
        request,
        objects,
        "usuarios/inicios_sesion.html",
        {"title": "Inicios de sesión", "NODO_BREADCRUMB": "usuarios:usuarios"},
    )


def importar_usuarios(request):
    """
    Importar usuarios.
    """
    form = ImportarUsuariosForm()
    historial = ImportadoDatos.objects.filter(
        tipo=IMPORTADO_USUARIOS
    ).order_by("-creado")[:15]
    if request.method == "POST":
        form = ImportarUsuariosForm(request.POST, request.FILES)

        if form.is_valid():
            archivo = form.cleaned_data["archivo"]
            cambiar_password = form.cleaned_data["cambiar_password"]

            errores = []
            inicio = timezone.now()
            hoja, error = get_hoja_excel(archivo.open(), nc=11)
            usuarios_modificados = 0
            usuarios_creados = 0

            if error:
                return error_response(request, error)

            for i in range(1, hoja.nrows):
                username = xls_text_value(hoja.cell(i, 0)).lower()
                password = xls_text_value(hoja.cell(i, 1))
                nombre = xls_text_value(hoja.cell(i, 2))
                apellidos = xls_text_value(hoja.cell(i, 3))
                email = xls_text_value(hoja.cell(i, 4))
                telefono = xls_text_value(hoja.cell(i, 5))
                telefono_extension = xls_text_value(hoja.cell(i, 6))
                cr = xls_text_value(hoja.cell(i, 7))
                roles = xls_text_value(hoja.cell(i, 8))
                zona_horaria = xls_text_value(hoja.cell(i, 10))

                if not username:
                    errores.append(
                        "[Fila {}] El nombre de usuario no fué "
                        "especificado. ".format(i)
                    )
                    continue

                if cr:
                    codigo_cr, nombre_cr = cr.split(" - ")
                    cr = CentroResponsabilidad.objects.filter(
                        codigo=codigo_cr,
                        nombre=nombre_cr,
                    ).first()
                    if not cr:
                        errores.append(
                            "[Fila {}] El centro de responsabilidad especificado "
                            "no existe. ".format(i)
                        )
                else:
                    cr = None
                    
                if email:
                    if (
                        User.objects.filter(email=email)
                        .exclude(username=username)
                        .exists()
                    ):
                        errores.append(
                            "[Fila {}] Ya existe un usuario con el "
                            "email {}".format(i, email)
                        )
                        continue

                usuario = User.objects.filter(username=username).first()
                creado = False
                if not usuario:
                    if not password:
                        errores.append(
                            "[Fila {}] La contraseña del usuario no fué "
                            "especificada. ".format(i)
                        )
                        continue

                    creado = True
                    usuario = User()
                    usuario.username = username
                    usuario.email = email
                    usuario.first_name = nombre
                    usuario.last_name = apellidos
                    usuario.set_password(password)
                    usuario.usuario_creado = request.user
                    usuario.save()
                    usuarios_creados += 1
                else:
                    usuario.usuario_modificado = request.user
                    usuarios_modificados += 1

                if not creado and cambiar_password:
                    if password:
                        usuario.set_password(password)
                        usuario.save()
                    else:
                        errores.append(
                            "[Fila {}] La contraseña del usuario no fué "
                            "especificada. ".format(i)
                        )
                        continue

                perfil = get_perfil(usuario)
                perfil.centro_responsabilidad = cr
                perfil.telefono = telefono
                perfil.telefono_extension = telefono_extension

                if zona_horaria:
                    if zona_horaria in [x for x, y in perfil.MX_TIMEZONES]:
                        perfil.zona_horaria = zona_horaria
                    else:
                        errores.append(
                            "[Fila {}] La zona horaria {} es inválida. ".format(
                                i, zona_horaria
                            )
                        )
                        
                perfil.save()


                error_roles = False
                instancias_roles = []
                for nombre_rol in roles.split(", "):
                    if nombre_rol:
                        rol = Rol.objects.filter(nombre__iexact=nombre_rol).first()
                        if not rol:
                            error_roles = True
                            errores.append(
                                "[Fila {}] No existe el rol con el "
                                "nombre {}".format(i, nombre_rol)
                            )
                            continue
                        instancias_roles.append(rol)

                if not error_roles:
                    perfil.roles.clear()
                    perfil.roles.add(*instancias_roles)

            mensaje = "{} usuarios creados - {} usuarios modificados.".format(
                usuarios_creados, usuarios_modificados
            )
            importado = ImportadoDatos.objects.create(
                tipo=IMPORTADO_USUARIOS,
                archivo=archivo,
                usuario_creado=request.user,
                usuario_modificado=request.user,
                log={
                    "inicio": inicio.isoformat(),
                    "fin": timezone.now().isoformat(),
                    "mensaje": mensaje,
                    "errores": errores,
                },
            )
            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
            return redirect("usuarios:importar_usuarios")

    return render(
        request,
        "usuarios/importar_usuarios.html",
        {
            "title": "Importar usuarios",
            "form": form,
            "historial": historial,
            "NODO_BREADCRUMB": "usuarios:usuarios",
        },
    )
