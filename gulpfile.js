'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var compass = require('gulp-compass');
var minifyCSS = require('gulp-minify-css');
var path = require('path');

var sassPath = './app/static/app/scss/**/*.scss';

gulp.task('sass', function () {
	return gulp.src(sassPath)
		.pipe(sass().on('error', sass.logError))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./app/static/app/css'));
});
 
gulp.task('sass:watch', function () {
	gulp.watch(sassPath, ['sass']);
});