from django.core.files.base import ContentFile
from django import forms
from .functions import validate_file
import os
import base64
import uuid
import six
import binascii
from django.conf import settings
from django.template.defaultfilters import filesizeformat

class CustomFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        self.image = kwargs.pop("image", False)
        self.pdf = kwargs.pop("pdf", False)
        self.zipfiles = kwargs.pop("zipfiles", False)
        self.excel = kwargs.pop("excel", False)
        self.word = kwargs.pop("word", False)
        self.csv = kwargs.pop("csv", False)
        super(CustomFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(CustomFileField, self).clean(*args, **kwargs)
        # path va a tener valor una vez que ya tenga adjunto algo
        # y ya no es necesario validar de nuevo el tipo de archivo
        path = getattr(data, "path", None)

        if data and not path:
            archivo_valido, mimes_validos = validate_file(
                data,
                imagen=self.image,
                pdf=self.pdf,
                zipfiles=self.zipfiles,
                excel=self.excel,
                word=self.word,
                csv=self.csv,
                return_mimes_validos=True,
            )

            if not archivo_valido:
                raise forms.ValidationError(
                    "El archivo adjunto es inválido, los archivos válidos "
                    "son: {}".format(mimes_validos)
                )

            """
            if data.size > settings.MAX_UPLOAD_SIZE:
                raise forms.ValidationError(
                    "El tamaño máximo para subir archivos es {}, "
                    "el archivo adjuntado es de {}".format(
                        filesizeformat(settings.MAX_UPLOAD_SIZE),
                        filesizeformat(data.size),
                    )
                )
            """
                
        return data
