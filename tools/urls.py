from django.urls import path, include, re_path
from . import views

app_name = "tools"


urlpatterns = [
    path("ajax/get_colonias/", views.get_colonias, name="get_colonias"),
]