from django.apps import AppConfig


class MiscToolsConfig(AppConfig):
    name = 'tools'
