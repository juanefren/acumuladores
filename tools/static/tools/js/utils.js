function reverse(s){
    return s.split("").reverse().join("");
}

function validarEmail(email, callback) {
    var url = 'https://neored.com/validador/service/'
    var key = '060R23-NEO'

    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if (!regex.test(email)){
        return callback(false);
    }

    var f = new FormData()
    f.append('key', key)
    f.append('email', email)

    $.ajax({
        url: url, 
        type: 'POST', 
        data: f, 
        processData: false, 
        contentType: false
    })
    .done(function(r) { 
        return callback(r == '"valid"')
    })
    .fail(function(){
        alert('Error al validar el servicio')
    })
}

function validarRFC(rfc) {
    var regex = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/;

    if (!regex.test(rfc)){
        return "El RFC no cuenta con una estructura válida."
    }

    var datestr = reverse(reverse(rfc).substring(3,9))

    var daystr = datestr.substring(4, 6)
    var monthstr = datestr.substring(2, 4)
    
    var day = parseInt(daystr)
    var month = parseInt(monthstr)

    if( day == 0 || day > 31){
        return "El valor " + daystr + "no corresponde a un día del mes." 
    }
    if(month == 0 || month > 12){
        return "El valor " + monthstr + "no corresponde a un mes del año." 
    }
}


function validarCURP(curp) {
    var regex = /^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$/;

    if (!regex.test(curp)){
        return "El CURP no cuenta con una estructura válida."
    }

    var datestr = curp.substring(4, 10);

    var daystr = datestr.substring(4, 6)
    var monthstr = datestr.substring(2, 4)
    
    var day = parseInt(daystr)
    var month = parseInt(monthstr)

    if( day == 0 || day > 31){
        return "El valor " + daystr + "no corresponde a un día del mes." 
    }
    if(month == 0 || month > 12){
        return "El valor " + monthstr + "no corresponde a un mes del año." 
    }
}

function quillGetHTML(inputDelta) {
    var tempCont = document.createElement("div");
    (new Quill(tempCont)).setContents(inputDelta);
    return tempCont.getElementsByClassName("ql-editor")[0].innerHTML;
}

function validarAutorizacion(id, password, callback) {
    if (!id || id == "") {
        throw Error("El id del usuario no fué especificado")
    }

    var url = window.urls.ajax_validar_autorizacion.replace("00", id)
    $.ajax({
        url: url,
        type: "POST",
        data: {password: password}
    }).done(function(response) {
        if (callback) {
            callback(response.status === "success")
        }
    })
}

function esPersonaFisica(rfc) {
    return (rfc || "").length === 13
}

function setBadgeInfoRegimen($element, rfc) {
    var regimen = "Persona Física"
    var cssClass = "badge-fisica"
    
    if(!esPersonaFisica(rfc)) {
        cssClass = "badge-moral"
        regimen = "Persona Moral"
    }
    
    $element.addClass(cssClass)
    $element.text(regimen)
}

function copyToClipboard(str) {
    var el = document.createElement('textarea');
    alertify = alertify || false
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (alertify) {
        alertify.info("Se ha copiado al <b>" + str + "</b> portapapeles");
    }
}

function to_decimal(num){
    if(!num) return 0;
    num = parseFloat(num.toString().replace(/\$|\,/g,''));
    if(isNaN(num)) return 0; 
    else return num;
}

/*
Selecciona los input checkbox que estén en pantalla, opcionalmente se puede 
pasar class para solo seleccionar algunos en específico.
*/
function toggle_checkboxes(selector){

    if(selector == undefined){
        selector = ""
    }

    var checkBoxes = $("input:checkbox" + selector)
    checkBoxes.prop("checked", !checkBoxes.prop("checked"));
}