#!/usr/bin/env python
import sys


class WebColor(object):
    @classmethod
    def from_HSL(cls, h, s, l):
        c = WebColor()
        c.hsl = (h, s, l)
        return c

    @classmethod
    def parse(cls, c):
        error_msg = "Color shuld be in html format (#rrggbb) or a rgb triple"
        c = c.strip()
        if "," in c:
            try:
                r, g, b = (int(i) for i in c.split(","))
            except ValueError:
                raise ValueError(error_msg)
            return r / 255.0, g / 255.0, b / 255.0
        elif c.startswith("#"):
            c = c.lstrip("#")
            if len(c) == 3:
                r, g, b = c
            elif len(c) == 6:
                r, g, b = c[:2], c[2:4], c[4:]
            else:
                raise ValueError(error_msg)
            try:
                r = int(r, 16)
                g = int(g, 16)
                b = int(b, 16)
            except ValueError:
                raise ValueError(error_msg)
            return r / 255.0, g / 255.0, b / 255.0
        else:
            raise ValueError(error_msg + ": " + c)

    def __init__(self, r=None, g=None, b=None):

        if r is None:
            self._r = self._g = self._b = 0.5
        elif hasattr(r, "r"):
            self._set(r._r, r._g, r._b)
        elif isinstance(r, str):
            r, g, b = self.parse(r)
            self._set(r, g, b)
        elif hasattr(r, "__iter__"):
            r, g, b = r
            self._set(r, g, b)
        elif g is None and b is None:
            self._set(r, r, r)
        else:
            self._set(r or 0, g or 0, b or 0)

    def copy(self):
        return WebColor(self._r, self._g, self._b)

    def _set(self, r, g, b):
        self._r = r
        self._g = g
        self._b = b

    def _get_r(self):
        return self._r

    def _set_r(self, r):
        self._r = r
        return self

    r = property(_get_r, _set_r, "Red component")

    def _get_g(self):
        return self._g

    def _set_g(self, g):
        self._g = g
        return self

    g = property(_get_g, _set_g, "Green component")

    def _get_b(self):
        return self._b

    def _set_b(self, b):
        self._b = b
        return self

    b = property(_get_b, _set_b, "Green component")

    def __str__(self):
        def component(c):
            return int(max(min(c * 255.0, 255.0), 0.0))

        return "#%02x%02x%02x" % (
            component(self._r),
            component(self._g),
            component(self._b),
        )

    def __unicode__(self):
        return unicode(str(self))

    def __repr__(self):
        return "WebColor(%i, %i, %i)" % (self.r, self.g, self.b)

    def __add__(self, v):
        return WebColor(self._r + v._r, self._g + v._g, self._b + v._b)

    def __sub__(self, v):
        return WebColor(self._r - v._r, self._g - v._g, self._b - v._b)

    def __mul__(self, v):
        return WebColor(self._r * v, self._g * v, self._b * v)

    def _get_css(self):
        def component(c):
            return max(min(c * 255.0, 255.0), 0.0)

        return "rgb(%i, %i, %i)" % (
            component(self.r),
            component(self.g),
            component(self.b),
        )

    css = property(_get_css)

    def _get_HSL(self):
        r = self._r
        g = self._g
        b = self._b

        var_min = min(r, g, b)
        var_max = max(r, g, b)
        del_max = var_max - var_min

        L = (var_max + var_min) / 2
        if del_max == 0:
            return 0.0, 0.0, 0.0

        if L < 0.5:
            S = del_max / (var_max + var_min)
        else:
            S = del_max / (2 - var_max - var_min)

        del_r = (((var_max - r) / 6.0) + (del_max / 2.0)) / del_max
        del_g = (((var_max - g) / 6.0) + (del_max / 2.0)) / del_max
        del_b = (((var_max - b) / 6.0) + (del_max / 2.0)) / del_max
        if r == var_max:
            H = del_b - del_g
        elif g == var_max:
            H = (1.0 / 3.0) + del_r - del_b
        elif b == var_max:
            H = (2.0 / 3.0) + del_g - del_r
        if H < 0.0:
            H += 1.0
        if H > 1.0:
            H -= 1.0
        return H, S, L

    @classmethod
    def _hue_2_rgb(cls, v1, v2, vH):
        if vH < 0.0:
            vH += 1.0
        if vH > 1.0:
            vH -= 1.0
        if (6.0 * vH) < 1.0:
            return v1 + (v2 - v1) * 6.0 * vH
        if (2.0 * vH) < 1.0:
            return v2
        if (3.0 * vH) < 2.0:
            return v1 + (v2 - v1) * ((2.0 / 3.0) - vH) * 6.0
        return v1

    def _set_HSL(self, t):
        h, s, l = t
        if s == 0.0:
            self._r = self._g = self._b = l
            return
        if l < 0.5:
            var_2 = l * (1.0 + s)
        else:
            var_2 = (l + s) - (s * l)
        var_1 = 2 * l - var_2

        hue_2_rgb = self._hue_2_rgb
        self._r = hue_2_rgb(var_1, var_2, h + (1.0 / 3.0))
        self._g = hue_2_rgb(var_1, var_2, h)
        self._b = hue_2_rgb(var_1, var_2, h - (1.0 / 3.0))

    hsl = property(_get_HSL, _set_HSL)

    def _get_h(self):
        h, s, l = self.hsl
        return h

    def _set_h(self, set_h):
        h, s, l = self.hsl
        self.hsl = set_h, s, l
        return self

    h = property(_get_h, _set_h)

    def _get_s(self):
        h, s, l = self.hsl
        return s

    def _set_s(self, set_s):
        h, s, l = self.hsl
        self.hsl = h, set_s, l
        return self

    s = property(_get_s, _set_s)

    def _get_l(self):
        h, s, l = self.hsl
        return l

    def _set_l(self, set_l):
        h, s, l = self.hsl
        self.hsl = h, s, set_l
        return self

    l = property(_get_l, _set_l)

    def blend(self, c2, interpolant):

        r = self._r
        g = self._g
        b = self._b
        r = r + (c2.r - r) * interpolant
        g = g + (c2.g - g) * interpolant
        b = b + (c2.b - b) * interpolant
        return WebColor(r, g, b)


if __name__ == "__main__":
    c = WebColor(0.2, 0.6, 0.8)

    def frange(steps):
        for n in range(steps + 1):
            yield float(n) / steps

    html = []
    html.append(
        """<html><body><table border="0" cellpadding="0" cellspacing="0">"""
    )

    c = WebColor(0.2, 0.9, 0.1)
    h = c.h
    STEPS = 20
    for s in frange(STEPS):
        html.append("""<tr>""")
        for l in frange(STEPS):
            c.hsl = (h, s, l)
            html.append(
                '<td style="width:10px;height:10px;background-color:%s"></td>'
                % c
            )
        html.append("</tr>")

    html.append("</table></body></html>")

    html = "\n".join(html)

    file("hsl.html", "w").write(html)
    WebColor.from_HSL(0.333, 1.0, 0.5)
