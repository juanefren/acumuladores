import sys, io, os, xlsxwriter, datetime
from .functions import date_to_str
from django.http import HttpResponse


class ExcelFile:

    n_row = 0
    n_col = 0
    sizes = {}

    def __init__(self, *args, **kwargs):

        output = kwargs.get("output")
        nombre_hoja = kwargs.get("nombre_hoja", "admintotal")

        try:
            nombre_hoja = nombre_hoja.decode("utf-8")
        except:
            pass

        self.output_xlsx = (
            io.BytesIO() if not output else output
        )  # io.BytesIO(output)
        self.wb = xlsxwriter.Workbook(
            self.output_xlsx, {"constant_memory": True}
        )
        self.ws = self.wb.add_worksheet(
            nombre_hoja
        )  # Nombre de Hoja de Trabajo

        self.XLWT_TITULO = self.wb.add_format({"bold": True})
        self.XLWT_TITULO_CENTER = self.wb.add_format(
            {"bold": True, "align": "center",}
        )
        self.XLWT_NUM = self.wb.add_format({"num_format": "#,##0.00"})
        self.XLWT_PORCIENTO = self.wb.add_format({"num_format": "0.00%"})
        self.DATE_FX = self.wb.add_format({"num_format": "DD/MM/YY"})
        self.XLWT_NUM_TITLE = self.wb.add_format(
            {"bold": True, "num_format": "#,##0.00",}
        )
        self.XLWT_NUM_TITLE_RED = self.wb.add_format(
            {"bold": True, "num_format": "#,##0.00", "font_color": "red"}
        )
        self.XLWT_NUM_RED = self.wb.add_format(
            {"num_format": "#,##0.00", "font_color": "red"}
        )
        self.XLWT_RED = self.wb.add_format({"font_color": "red"})
        self.filtros = kwargs.get("filtros", [])
        self.set_filtros()

    def set_filtros(self):

        if self.filtros:
            if type(self.filtros) == dict:
                self.filtros = self.filtros.values()

            for nombre, valor in self.filtros:
                if valor:
                    if isinstance(valor, (datetime.date, datetime.datetime)):
                        valor = date_to_str(valor)
                    self.write_str("%s: %s" % (nombre.title(), valor))

            self.add_row()

    def add_row(self, n=1):
        self.n_row += n
        self.n_col = 0

    def add_col(self, n=1):
        self.n_col += n

    def write(self, valor, style=None, **kwargs):
        colnumber = kwargs.get("colnumber")
        colspan = kwargs.get("colspan")

        if not colnumber:
            colnumber = self.n_col
        else:
            self.n_col = colnumber

        self.sizes[self.n_col] = max(
            len("%s" % valor), self.sizes.get(self.n_col, 0)
        )

        if colspan:

            self.ws.merge_range(
                self.n_row,
                self.n_col,
                self.n_row,
                self.n_col + (colspan - 1),
                valor,
                style,
            )
            self.n_col += colspan - 1

        self.ws.write(self.n_row, colnumber, valor, style)

        self.add_col()

    def clean_str(self, s):
        """
        Convierte a string vacío None y formatea con date_to_str las fechas
        """
        if s is None:
            return ""

        elif isinstance(s, (datetime.date, datetime.datetime)):
            return date_to_str(s)

        else:
            return str(s)

    def write_title(self, valor, **kwargs):
        self.write(self.clean_str(valor), self.XLWT_TITULO, **kwargs)

    def write_num(
        self,
        valor_decimal,
        cero_en_blanco=False,
        negativo=False,
        title=False,
        **kwargs
    ):
        if negativo and valor_decimal > 0:
            valor_decimal *= -1

        if cero_en_blanco and not valor_decimal:
            valor_decimal = ""

        if title:
            if negativo:
                style = self.XLWT_NUM_TITLE_RED
            else:
                style = self.XLWT_NUM_TITLE
        else:
            if negativo:
                style = self.XLWT_NUM_RED
            else:
                style = self.XLWT_NUM

        self.write(valor_decimal, style)

    def write_str(self, valor, **kwargs):
        self.write(self.clean_str(valor), **kwargs)

    def set_width(self):
        for i, c in self.sizes.items():
            w = c + c // 3 + 2
            self.ws.set_column(i, i, min(w, 35))

    def xlsx_to_response(self, filename):

        self.set_width()
        self.wb.close()
        self.output_xlsx.seek(0)

        if filename.endswith(".xls"):
            filename = filename[:-4]
        if filename.endswith(".xlsx"):
            filename = filename[:-5]

        response = HttpResponse(
            self.output_xlsx.read(),
            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        )
        response["Content-Disposition"] = 'attachment; filename="%s.xlsx"' % (
            filename.replace('"', "")
        )
        return response
