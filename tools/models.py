from django.db import models

ENTIDADES = (
    (1, "AGU", "Aguascalientes"),
    (2, "BCN", "Baja California"),
    (3, "BCS", "Baja California Sur"),
    (4, "CAM", "Campeche"),
    (5, "CHP", "Chiapas"),
    (6, "CHH", "Chihuahua"),
    (7, "COA", "Coahuila"),
    (8, "COL", "Colima"),
    (9, "DIF", "Ciudad de México"),
    (10, "DUR", "Durango"),
    (11, "GUA", "Guanajuato"),
    (12, "GRO", "Guerrero"),
    (13, "HID", "Hidalgo"),
    (14, "JAL", "Jalisco"),
    (15, "MEX", "Estado de México"),
    (16, "MIC", "Michoacán"),
    (17, "MOR", "Morelos"),
    (18, "NAY", "Nayarit"),
    (19, "NLE", "Nuevo León"),
    (20, "OAX", "Oaxaca"),
    (21, "PUE", "Puebla"),
    (22, "QUE", "Querétaro"),
    (23, "ROO", "Quintana Roo"),
    (24, "SLP", "San Luis Potosí"),
    (25, "SIN", "Sinaloa"),
    (26, "SON", "Sonora"),
    (27, "TAB", "Tabasco"),
    (28, "TAM", "Tamaulipas"),
    (29, "TLA", "Tlaxcala"),
    (30, "VER", "Veracruz"),
    (31, "YUC", "Yucatán"),
    (32, "ZAC", "Zacatecas"),
)

class Direccion(models.Model):
    municipio = models.CharField(max_length=255, null=False, blank=True, default="")
    localidad = models.CharField(max_length=255, null=False, blank=True, default="")
    estado = models.CharField(max_length=255, blank=True, default="")
    calle = models.CharField(max_length=500, null=True, blank=True, default="")
    colonia = models.CharField(max_length=100, null=True, blank=True, default="")
    cp = models.CharField(max_length=7, null=True, blank=True, default="")
    telefono = models.CharField(max_length=100, null=True, blank=True, default="")
    email = models.EmailField(blank=True, default="")
    numero_e = models.CharField(max_length=100, blank=True, null=False, default="")
    numero_i = models.CharField(max_length=100, blank=True, null=False, default="")
    pais_residencia = models.CharField(max_length=3, default="")
    
    # https://stackoverflow.com/questions/12504208/what-mysql-data-type-should-be-used-for-latitude-longitude-with-8-decimal-places
    latitud = models.DecimalField(max_digits=11, decimal_places=8, default=0, blank=True, null=True)
    longitud = models.DecimalField(max_digits=11, decimal_places=8, default=0, blank=True, null=True)
    referencia = models.CharField(max_length=500, null=True, blank=True, default="")
    
    #CLAVES PARA COMERCIO EXTERIOR
    ce_municipio = models.CharField(max_length=3, null=True, blank=True, default="")
    ce_localidad = models.CharField(max_length=3, null=True, blank=True, default="")
    ce_estado = models.CharField(max_length=3, blank=True, default="")
    ce_colonia = models.CharField(max_length=4, null=True, blank=True, default="")

    def __str__(self):
        d = self.calle
        if self.numero_e:
            d += " N° %s" % self.numero_e
        
        if self.numero_i:
            d += "-%s" % self.numero_i
        
        if not d:
          return ""
        return d


    def set_latlng(self, api_key=None):
      import requests

      if self.latitud and self.longitud:
        return False

      if not api_key:
        from admintotal.models import ConfiguracionGeneral

        api_key = ConfiguracionGeneral.objects.values_list(
            'google_api_key', 
            flat=True
        ).first()

        if not api_key:
          api_key = settings.AT_MAPS_KEY

      try:
          r = requests.get(
              'https://maps.googleapis.com/maps/api/geocode/json', 
              params={
                'key': api_key,
                'address': self.get_address_for_google_maps()
              },
              timeout=5,
          )

          if hasattr(r, 'json'):
              res = r.json()
              if res['status'] == 'OK' and 'results' in res:
                  first_result = res['results'].pop(0)
                  self.latitud = first_result['geometry']['location']['lat']
                  self.longitud = first_result['geometry']['location']['lng']
                  self.save()
                  return True
                  
          return False

      except requests.exceptions.Timeout:
          return False
      
      except requests.exceptions.RequestException:
          return False
      
      return False

    def to_dict(self, compacta=False):
      if not self.id:
        return {}

      if compacta:
        return {
          'id': self.id,
          'telefono': self.telefono,
          'email': self.email,
          'direccion_completa': self.direccion_corta(),
          'lat': str(self.latitud) if self.latitud else 0,
          'lng': str(self.longitud) if self.longitud else 0,
        }

      return {
        'id': self.id,
        'municipio': self.municipio,
        'localidad': self.localidad,
        'estado': self.estado,
        'calle': self.calle,
        'colonia': self.colonia,
        'cp': self.cp,
        'telefono': self.telefono,
        'email': self.email,
        'numero_e': self.numero_e,
        'numero_i': self.numero_i,
        'pais_residencia': self.pais_residencia,
        'direccion_completa': self.direccion_corta(),
        'lat': str(self.latitud) if self.latitud else 0,
        'lng': str(self.longitud) if self.longitud else 0,
      }

    def get_address_for_google_maps(self):
      if not self.cp:
        return ''
        
      return ' '.join([
          self.calle,
          self.numero_e,
          self.colonia,
          self.cp,
          self.localidad + ',',
          self.estado
      ])
    
    def get_maps_link(self):
        if self.latitud:
            return "https://maps.google.com/maps?q=%s,%s" % (
                self.latitud,
                self.longitud
            )

        return "https://maps.google.com/maps?q=%s" % (self.direccion_corta())

    def colonia_ciudad(self):
        out = ""
        if self.colonia:
            out += ' Col. %s' % self.colonia
            
        if self.cp:
            out += ' C.P. %s' % self.cp
            
        if self.municipio:
            out += ' %s' % self.municipio
        
        if self.estado:
            out += ' %s' % self.estado
        return out
    #pais = CountryField()
    
    def ciudad(self):
        return Foo(c=self.municipio, e=self.estado)
    
    def ciudad_delegacion(self) :
        if self.estado == "MEXICO DF":
            return "Delegación"
        return "Ciudad"
    
    def complete(self, completa=True):
        out = self.calle
        
        if self.numero_e:
            out += ' N° %s' % self.numero_e
            if self.numero_i:
                out += '-%s' % self.numero_i
        
        if self.colonia > '':
            out += u' Col. %s' % self.colonia
            
        if self.cp and self.cp > '':
            out += ' C.P. %s' % self.cp
            
        if self.municipio:
            out += u' %s,' % self.municipio
        
        if self.estado:
            out += u' %s.' % self.estado
        
        if completa:
            if self.telefono:
                out += ' Tel %s' % self.telefono
                
            if self.email:
                out += ' ' + self.email
                
        return out

    def direccion_corta(self, completa=True):
        return self.complete(completa=False)
        
    def direccion_larga(self):
        out = self.calle + " " + self.numero_e + " " + self.numero_i
        
        out += "<div>" 
        if self.colonia:
            out += 'Col. %s' % self.colonia
        
        if self.cp:
            out += ' C.P. %s' % self.cp
        out += "</div><div>"

        if self.municipio:
            out += '%s,' % self.municipio
        
        if self.estado:
            out += ' %s.' % self.estado
        out += "</div><div>"
        if self.telefono:
            out += 'Tel: %s' % self.telefono
        out += "</div>"
            
        return mark_safe(out)

    
    def get_clave_estado(self):
        from .functions import reemplazar_acentos
        if self.estado.lower() == "distrito federal":
          return "DIF"

        if self.estado.upper() == "MÉXICO" or self.estado.lower() == "méxico" or self.estado.lower() == "mexico":
          return "MEX"

        estado = self.estado.lower()
        for entidad in ENTIDADES:
            entidad_catalogo = entidad[2].lower()
            if entidad_catalogo in estado or  reemplazar_acentos(entidad_catalogo) in estado:
                return entidad[1]
        return self.estado.upper()


    def get_direccion_geoc(self):
        return "%s %s, %s, %s %s, %s" % (self.calle, self.numero_e, self.colonia, self.cp, self.municipio, self.estado)
    
    def show_ciudad(self):
        return "%s, %s" % (self.municipio, self.estado)
   
    def save(self, *args, **kwargs):
        if self.colonia:
          self.colonia = self.colonia.strip()
          
        if self.calle:
          self.calle = self.calle.strip()
          
        if self.municipio:
          self.municipio = self.municipio.strip()
          
        if self.localidad:
          self.localidad = self.localidad.strip()
          
        if self.estado:
          self.estado = self.estado.strip()
          
        return super(Direccion, self).save(*args, **kwargs)
