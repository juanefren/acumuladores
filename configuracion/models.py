import pytz
from django.db import models
from django.utils import timezone

class Configuracion(models.Model):
    MX_TIMEZONES = [(x, x) for x in pytz.country_timezones["MX"]]
    zona_horaria = models.CharField(
        max_length=255,
        choices=MX_TIMEZONES,
        null=True,
        blank=True,
        default="America/Mexico_City",
        verbose_name="Zona Horaria",
    )
    project_name = models.CharField(max_length=255, blank=True, null=True)
    logo = models.ImageField(upload_to="conf/", null=True, blank=True)
    color_empresa = models.CharField(
        max_length=10, blank=True, null=True, default="#F96302"
    )
    theme_primary_color = models.CharField(
        max_length=10, blank=True, null=True, default="#F96302"
    )
    theme_secondary_color = models.CharField(
        max_length=10, blank=True, null=True, default="#989898"
    )
    api_key = models.CharField(max_length=500, default='', blank=True, null=True, verbose_name='API-KEY')
    fecha_inicio = models.DateField(default=timezone.now)
    saldo_inicial = models.DecimalField(decimal_places=2, max_digits=12, default=0)



