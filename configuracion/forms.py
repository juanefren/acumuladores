from django import forms
from django.forms.models import ModelChoiceField

from tools.forms import BootstrapModelForm, BootstrapForm, CalendarDateField

from .models import (
    Configuracion,
   
)   

class ConfiguracionForm(BootstrapModelForm):
    class Meta:
        model = Configuracion
        fields = (
            "project_name",
            "logo",
            "color_empresa",
            "theme_primary_color",
            "theme_secondary_color",
            "zona_horaria",
            "api_key",
            "fecha_inicio",
            "saldo_inicial",
            )
        
    fecha_inicio = CalendarDateField()