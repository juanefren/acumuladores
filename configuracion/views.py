import pprint
ap = pprint.PrettyPrinter(indent=4)

import json
import datetime

from io import TextIOWrapper
from functools import partial, wraps

from django.forms.models import modelformset_factory
from django.http import Http404
from django.contrib import messages
from django.db.models import Q, Sum
from django.urls import reverse
from django.conf import settings
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt


from tools.classes import ExcelFile
from tools.forms import EMPTY_CHOICE, ELEMENTOS_PAGINA
from tools.functions import (
    json_response, 
    error_response, 
    list_view, 
    to_int, 
    safe_division, 
    render_pdf,
)
from .models import (
    Configuracion,
)
from .forms import (

    ConfiguracionForm,
)


def configuracion(request, tipo=None):
    try:
        configuracion = Configuracion.objects.get()
    except:
        configuracion = Configuracion.objects.get_or_create()[0]

    form = ConfiguracionForm(instance=configuracion)    
    if request.method == "POST":
        form = ConfiguracionForm(request.POST, request.FILES, instance=configuracion)
        if form.is_valid():
            form.save()
            messages.success(
                request, "Los datos han sido guardados correctamente."
            )
        return redirect(".")

    return render(
        request,
        "configuracion/configuracion.html",
        {
            "title": "",
            "form":form,
            "NODO_BREADCRUMB": "configuracion:configuracion",
        },
    )
    