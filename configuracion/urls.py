from django.urls import path
from . import views

app_name  = "configuracion"
urlpatterns = [
    #Definiciones
    path(
        "configuracion/personalizacion_de_sistema/", 
        views.configuracion, {}, 
        "configuracion"
    ),
]