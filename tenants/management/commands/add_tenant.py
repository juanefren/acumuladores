from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from tenants.models import Tenant, Domain
from django_tenants.utils import schema_context
from django.contrib.auth.models import User
from usuarios.functions import get_perfil
import io

class Command(BaseCommand):
    help = 'Crea un nuevo tenant'

    def add_arguments(self, parser):
        parser.add_argument('schema_name', type=str)

    def handle(self, *args, **options):
        schema_name = options["schema_name"]
        tenant = Tenant.objects.filter(schema_name=schema_name).first()
        if tenant:
            self.stderr.write(
                f"Ya existe un tenant con schema_name {schema_name}"
            )
            return False

        tenant = Tenant.objects.create(schema_name=schema_name)
        domain = Domain.objects.create(tenant=tenant, domain=f"{schema_name}.{settings.DOMAIN}")
            
        with schema_context(tenant.schema_name):
            u = User(is_superuser=True, is_staff=True)
            u.username = "admin"
            u.set_password("1234")
            u.save()
            get_perfil(u)
            self.stdout.write('-' * 30)
            self.stdout.write(
                "Se creó el superusuario 'admin' con password '1234'"
            )
            self.stdout.write('-' * 30)
        self.stdout.write(
            self.style.SUCCESS(
                f"Se ha creado el tenant http://{domain.domain}"
            )
        )

"""
from tenants.models import Tenant
for t in Tenant.objects.exclude(schema_name="public"):
    t.delete()
"""
