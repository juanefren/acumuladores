from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from tenants.models import Tenant, Domain
from django.core.management import call_command
import io

class Command(BaseCommand):
    help = 'Inicializa el proyecto, configurando el tenant public'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        call_command("migrate", stdout=io.StringIO())

        public = Tenant.objects.filter(schema_name="public").first()
        if public:
            self.stderr.write("Ya existe un tenant con schema_name public")
            return False

        public = Tenant.objects.create(schema_name="public")
        Domain.objects.create(tenant=public, domain=settings.DOMAIN)
        self.stdout.write(
            self.style.SUCCESS(
                "Se ha configurado el tenant public para el proyecto"
            )
        )