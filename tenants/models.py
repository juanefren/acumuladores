from django.db import models
from django.contrib.auth.models import User
from django_tenants.models import TenantMixin, DomainMixin
from django.conf import settings
from django_tenants.utils import schema_context
from django.urls import reverse
from django_tenants.urlresolvers import reverse as django_tenants_reverse


class Tenant(TenantMixin):
    creado = models.DateField(auto_now_add=True)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

    def absolute_url(self, url_name=None, urls_args=[], https=False):
        proto = "http"
        if https:
            proto = "https"
        url = f"{proto}://{self.schema_name}.{settings.DOMAIN}"
        if url_name:
            path = django_tenants_reverse(
                url_name, 
                urlconf=settings.ROOT_URLCONF, 
                args=urls_args,
            )
            url = url + path
        return url

    def create_domains(self):
        if not self.id:
            raise Exception("Es necesario guardar la instancia para continuar")

        domains = [
            f"{self.schema_name}.{settings.DOMAIN}",
            f"{self.schema_name}.localhost",
        ]

        for domain in domains:
            Domain.objects.get_or_create(tenant=self, domain=domain)
        
        return True


class Domain(DomainMixin):
    pass
