from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'command/', views.command),
    url(r'command_externo/', views.command_externo),
    url(r'configuracion/', views.editar_configuracion),
]
