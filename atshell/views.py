from django.contrib.auth.decorators import login_required, permission_required
from django.db import models
from django.shortcuts import render, HttpResponse, redirect
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from .models import Configuracion as ATshellConf

class anyimport(object):
    def __init__(self, importmodule, scope):
        exec (importmodule, scope)

@login_required()
def command(request):
    if not request.user.is_superuser:
        return HttpResponse("No autorizado")
    
    dict = {}
    error = ''
    query = ''
    objects = []
    
    #errorIp = get_valido_command(request)

    #if errorIp:
    #    from tools.views import error_view
    #    return error_view(request, errorIp)
        
    if request.method == 'POST':
        objects, error = ejecutar(request)  
        
    #comments = '''# _ = objects.append \n# _mov = Movimiento.objects.get \n# _movs = Movimiento.objects.filter\n'''
    comments = ''
    return render(request, 'atshell/command.html', { 
        'objects':objects, 
        'query':request.POST.get("query", comments), 
        'error':error 
    }) 

def command_externo(request):
    
    errorIp = get_valido_command(request)

    if errorIp:
        return HttpResponse(errorIp)

    cadena = "%s%s" % (
        request.POST["query"],
        settings.ADMINTOTAL_KEY,
    ) 

    if not dmspitic_key_valida(cadena, request.POST["cadenaEncriptada"]):
        return HttpResponse("Clave incorrecta.")

    objects,error = ejecutar(request)
    if error:
        return HttpResponse(error)

    objectsText = ""
    for o in objects:
        objectsText += "<div>%s</div>" % o

    return HttpResponse(objectsText)

def ejecutar(request):
    from builtins import str

    try:
        configuracion = ATshellConf.objects.get()
    except ObjectDoesNotExist:
        configuracion = ATshellConf.objects.get_or_create(antes="")[0]
    
    objects = []
    query = request.POST.get('query', '')
    q = query.replace("\r","")
    q += "\npass"
    error = ""

    # helpers
    _ = objects.append
    cmd_to_execute = "\n".join([configuracion.antes, q, configuracion.despues])

    if request.POST.get("activar_try"):
        try:
            exec(cmd_to_execute)
        except Exception as e:
            error = u"%s %s" % (str(e.__class__), str(e))

    else:
        exec(cmd_to_execute)
    
    exec(configuracion.despues)

    return objects,error


def get_valido_command(request):
    #from tools.functions import get_user_ip_address
    #ip = get_user_ip_address(request)
    
    #from clients.models import IpAtshell
    return True

    """
    if getattr(settings, 'AT_SHELL_IP_VALIDATE', True):
        valido = IpAtshell.objects.using("central").filter(
            Q(ip=ip)&
            Q(
                Q(cliente__isnull=True)|
                Q(cliente__clave=get_clave())
            )
        ).distinct().exists()
        if not valido and ip != "127.0.0.1":
            return u"IP %s no válida" % ip

        return False
    return None"""


@permission_required("admin")
def editar_configuracion(request):

    try:
        configuracion = ATshellConf.objects.get()
    except ObjectDoesNotExist:
        configuracion = ATshellConf.objects.get_or_create(antes="")[0]

    if request.method == "POST":

        configuracion.antes = request.POST.get("antes")
        configuracion.despues = request.POST.get("despues")
        configuracion.save()
        return redirect(".")
    
    return render(
        request,
        'atshell/configuracion.html',
        {"configuracion":configuracion, },
    )
